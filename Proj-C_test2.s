# Proj-C_test2.s

.data
Arr:.word   5,7,2,6,8,3,4,9,1,10

.text

lui $8,4097
andi $1, $0, 0


scan:
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

    sll $9, $1, 2
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

    add $9, $8, $9
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

    lw $2, 0($9)
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

    beq $2, $0 main
	sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op
    addi $1, $1, 1
    addi $9, $9, 4
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op
    j scan
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

main:
    andi $3, $0, 0


loop1:
    addi $2, $1, -1
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

    slt $2, $3, $2
    sll $0,$0, 0 #No op
	sll $0,$0, 0 #No op

    beq $2, $0 exit
	sll $0,$0, 0 #No op
	sll $0,$0, 0 #No op
    andi $4, $0, 0


loop2:
    sub $2, $1, $3
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

    addi $2, $2, -1
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op


    slt $2, $4, $2
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

    beq $2, $0 Loop4
    sll $0,$0, 0 #No op
    sll $9, $4, 2
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

   add $9, $9, $8
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

    lw $5, 0($9)
    lw $6, 4($9)
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

    slt $2, $6, $5
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

    beq $2, $0 Loop3
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op
    sw $6, 0($9)
    sw $5, 4($9)


Loop3:
    addi $4, $4, 1
    j loop2
	sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op

Loop4:
    addi $3, $3, 1
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op
    j loop1
    sll $0,$0, 0 #No op
    sll $0,$0, 0 #No op


exit:
    li   $v0, 10
    syscall  
