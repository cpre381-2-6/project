# Proj-C_test3.s

# data section
#.data
#base: .space 0

# code/instruction section
.text
	addi  $26, $0, 26
	addi  $1, $0, 1
	add   $2, $1, $1
	add   $2, $1, $1 
	add   $2, $1, $1 
	add   $2, $1, $1 
	
	lui   $24, 0x00001001
	ori   $25, $24, 0x00000000
	sw    $26, 0($25) 
	lw    $27, 0($25)
	sw    $27, 4($25)
	sw    $27, 8($25)
	sw    $27, 12($25)
	
	add   $4, $2, $2
	add   $8, $4, $4
	add   $8, $4, $4
	add   $8, $4, $4
	add   $8, $4, $4
	
	add   $16, $8, $8
	addi  $17, $16, 1
	addi  $17, $16, 1
	addi  $17, $16, 1
	addi  $17, $16, 1
	
	addi  $18, $17, 1
	addi  $19, $18, 1
	addi  $19, $18, 1
	addi  $19, $18, 1
	addi  $19, $18, 1
	
	lui   $1, 0x00001001
	ori   $2, $1, 0x00000000
	sw    $4, 0($2)
	sw    $4, 0($2)
	sw    $4, 0($2)
	sw    $4, 0($2)
	
	lui   $3, 0x00001001
	ori   $4, $3, 0x00000000
	add   $5, $4, $3
	sw    $5, 0($4)
	sw    $5, 0($4)
	sw    $5, 0($4)
	sw    $5, 0($4)
	
	addi  $1, $0, 7
	sw    $1, 4($4)
	lw    $7, 4($4)
	add   $14, $7, $7
	add   $14, $7, $7
	add   $14, $7, $7
	
	beq   $2, $2, bTest0
	add   $2, $1, $1
	add   $2, $1, $1
	add   $2, $1, $1
	
FAIL0:
	addi $7, $0, 7
	
bTest0:
	bne   $1, $2, bTest1
	addi  $1, $0, 1
	addi  $2, $0, 2
	addi  $3, $0, 3
	
FAIL1:
	addi $7, $0, 7
	
bTest1:
	addi  $4, $0, 4
	addi  $5, $0, 4
	addi  $6, $0, 6
	bne   $6, $5, bTest2
	
FAIL2:
	addi $7, $0, 7
	
bTest2:
	addi  $4, $0, 2
	addi  $5, $0, 2
	addi  $6, $0, 2
	beq   $5, $6, jTest0
	
FAIL3:
	addi $7, $0, 7
	
jTest0:
	j jTest1
	
jTest2:
	jr   $ra
	
FAIL4:
	addi $7, $0, 7
	
jTest1:
	jal jTest2

exit:
	addi  $2,  $0,  10 
	syscall