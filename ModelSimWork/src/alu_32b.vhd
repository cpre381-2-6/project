library IEEE;
use IEEE.std_logic_1164.all;

entity alu_32b is
   port(A        : in std_logic_vector(31 downto 0);
      	B        : in std_logic_vector(31 downto 0);
      	Control  : in std_logic_vector(2 downto 0);
        Unsign   : in std_logic;
      	Result   : out std_logic_vector(31 downto 0);
      	Carryout : out std_logic;
      	Overflow : out std_logic;
      	Zero     : out std_logic);
end alu_32b;

architecture structure of alu_32b is

component alu_1b
   port(A         : in std_logic;
      	B         : in std_logic;
      	B_Inverse : in std_logic;
      	Cin       : in std_logic;
      	Sel       : in STD_LOGIC_VECTOR (2 downto 0);
      	SLT_I     : in std_logic;
      	Carryout  : out std_logic;
      	Overflow  : out std_logic;
      	Result    : out std_logic;
        SLT_O     : out std_logic);
end component;

component mux_2
  port ( sel  : in  STD_LOGIC;
         i0   : in  STD_LOGIC;
         i1   : in  STD_LOGIC;
         o    : out STD_LOGIC);
end component;

signal B_Inverse, notCarryout, sltCarry, Sub : std_logic;

signal con : std_logic_vector(2 downto 0);

signal carry, output, over, slt : std_logic_vector(31 downto 0);

begin

process (Control)
	begin
	if Control = "111" then
		B_Inverse <= '1';
		con <= "101";
		Sub <= '1';
	elsif Control = "110" then
		B_Inverse <= '1';
		con <= Control;
		Sub <= '1';
	else
		B_Inverse <= '0';
		con <= Control;
		Sub <= '0';
	end if;
end process;

notCout: notCarryout <= not carry(31);

sltMux: mux_2 port map(Unsign, slt(31), notCarryout, sltCarry);

alu0: alu_1b port map(A(0), B(0), B_Inverse, Sub, con, sltCarry, carry(0), over(0), output(0), slt(0));

gen0: for i in 1 to 31 generate
  alu_1b_i: alu_1b port map(A(i), B(i), B_Inverse, carry(i-1), con, '0', carry(i), over(i), output(i), slt(i));
end generate;

resultO: Result <= output;

carryO: Carryout <= carry(31);

overflowO: Overflow <= over(31);

process (output)
	variable or_output : std_logic;
	begin
	or_output := '0';
	for i in 0 to 31 loop
		or_output := or_output or output(i);
	end loop;
	Zero <= not or_output;
end process;

end structure;
