library IEEE;
use IEEE.std_logic_1164.all;

entity register_file is
  port( clock         : in STD_LOGIC;
	reset         : in STD_LOGIC;
	d0            : out STD_LOGIC_VECTOR(31 downto 0);
	d1            : out STD_LOGIC_VECTOR(31 downto 0);
	reg2          : out STD_LOGIC_VECTOR(31 downto 0);
	r0            : in STD_LOGIC_VECTOR(4 downto 0);
	r1            : in STD_LOGIC_VECTOR(4 downto 0);
	register_data : in STD_LOGIC_VECTOR(31 downto 0);
	register_num  : in STD_LOGIC_VECTOR(4 downto 0);
        write_enable  : in STD_LOGIC);
end register_file;

architecture structure of register_file is

component decoder
    port ( write_enable : in  STD_LOGIC;
           A            : in  STD_LOGIC_VECTOR (4 downto 0);
           X            : out STD_LOGIC_VECTOR (31 downto 0));
end component;

component n_bit_register
  generic(N : integer := 32);
  port(i_CLK        : in STD_LOGIC;
       i_RST        : in STD_LOGIC;
       i_WE         : in STD_LOGIC;
       i_D          : in STD_LOGIC_VECTOR(N-1 downto 0);
       o_Q          : out STD_LOGIC_VECTOR(N-1 downto 0));
end component;

component mux
    port ( sel  : in  STD_LOGIC_VECTOR (4 downto 0);
           i0   : in  STD_LOGIC_VECTOR (31 downto 0);
           i1   : in  STD_LOGIC_VECTOR (31 downto 0);
           i2   : in  STD_LOGIC_VECTOR (31 downto 0);
           i3   : in  STD_LOGIC_VECTOR (31 downto 0);
           i4   : in  STD_LOGIC_VECTOR (31 downto 0);
           i5   : in  STD_LOGIC_VECTOR (31 downto 0);
           i6   : in  STD_LOGIC_VECTOR (31 downto 0);
           i7   : in  STD_LOGIC_VECTOR (31 downto 0);
           i8   : in  STD_LOGIC_VECTOR (31 downto 0);
           i9   : in  STD_LOGIC_VECTOR (31 downto 0);
           i10  : in  STD_LOGIC_VECTOR (31 downto 0);
           i11  : in  STD_LOGIC_VECTOR (31 downto 0);
           i12  : in  STD_LOGIC_VECTOR (31 downto 0);
           i13  : in  STD_LOGIC_VECTOR (31 downto 0);
           i14  : in  STD_LOGIC_VECTOR (31 downto 0);
           i15  : in  STD_LOGIC_VECTOR (31 downto 0);
           i16  : in  STD_LOGIC_VECTOR (31 downto 0);
           i17  : in  STD_LOGIC_VECTOR (31 downto 0);
           i18  : in  STD_LOGIC_VECTOR (31 downto 0);
           i19  : in  STD_LOGIC_VECTOR (31 downto 0);
           i20  : in  STD_LOGIC_VECTOR (31 downto 0);
           i21  : in  STD_LOGIC_VECTOR (31 downto 0);
           i22  : in  STD_LOGIC_VECTOR (31 downto 0);
           i23  : in  STD_LOGIC_VECTOR (31 downto 0);
           i24  : in  STD_LOGIC_VECTOR (31 downto 0);
           i25  : in  STD_LOGIC_VECTOR (31 downto 0);
           i26  : in  STD_LOGIC_VECTOR (31 downto 0);
           i27  : in  STD_LOGIC_VECTOR (31 downto 0);
           i28  : in  STD_LOGIC_VECTOR (31 downto 0);
           i29  : in  STD_LOGIC_VECTOR (31 downto 0);
           i30  : in  STD_LOGIC_VECTOR (31 downto 0);
           i31  : in  STD_LOGIC_VECTOR (31 downto 0);
           o    : out STD_LOGIC_VECTOR (31 downto 0));
end component;

signal decoder_out : STD_LOGIC_VECTOR(31 downto 0);

signal d0_Carry, d1_Carry : STD_LOGIC_VECTOR(31 downto 0);

type t_register_outputs is array (0 to 31) of STD_LOGIC_VECTOR(31 downto 0);

signal ro : t_register_outputs;

begin

line0: decoder port map (write_enable, register_num, decoder_out);

line1: n_bit_register port map(clock, '0', '1', x"00000000", ro(0));

line2: for i in 1 to 31 generate
  register_i: n_bit_register port map(clock, reset, decoder_out(i), register_data, ro(i));
end generate;

line3: mux port map (r0, ro(0), ro(1), ro(2), ro(3), ro(4), ro(5), ro(6), ro(7), ro(8), ro(9), ro(10), ro(11), ro(12), ro(13), ro(14), ro(15), ro(16), ro(17), ro(18), ro(19), ro(20), ro(21), ro(22), ro(23), ro(24), ro(25), ro(26), ro(27), ro(28), ro(29), ro(30), ro(31), d0_Carry);

line4: mux port map (r1, ro(0), ro(1), ro(2), ro(3), ro(4), ro(5), ro(6), ro(7), ro(8), ro(9), ro(10), ro(11), ro(12), ro(13), ro(14), ro(15), ro(16), ro(17), ro(18), ro(19), ro(20), ro(21), ro(22), ro(23), ro(24), ro(25), ro(26), ro(27), ro(28), ro(29), ro(30), ro(31), d1_Carry);

line5: reg2 <= ro(2);

P_IF: process (d0_Carry, d1_Carry, r0, r1, register_data, register_num, write_enable) is

begin

	if ((write_enable = '1') and (r0 = register_num)) then
		d0 <= register_data;
	else 
		d0 <= d0_Carry;
	end if;
	
	if ((write_enable = '1') and (r1 = register_num)) then
		d1 <= register_data;
	else 
		d1 <= d1_Carry;
	end if;

end process P_IF;

end structure;