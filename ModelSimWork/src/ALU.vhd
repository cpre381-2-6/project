library IEEE;
use IEEE.std_logic_1164.all;

entity ALU is
   port(A            : in std_logic_vector(31 downto 0);
        ALUorBS      : in std_logic;
        Arithmetic   : in std_logic;
      	B            : in std_logic_vector(31 downto 0);
      	Control      : in std_logic_vector(2 downto 0);
        Shift_Amount : in std_logic_vector(4 downto 0);
      	Shift_Left   : in std_logic;
        Unsign       : in std_logic;
      	Result       : out std_logic_vector(31 downto 0);
      	Carryout     : out std_logic;
      	Overflow     : out std_logic;
      	Zero         : out std_logic);
end ALU;

architecture structure of ALU is

component alu_32b
   port(A        : in std_logic_vector(31 downto 0);
      	B        : in std_logic_vector(31 downto 0);
      	Control  : in std_logic_vector(2 downto 0);
        Unsign   : in std_logic;
      	Result   : out std_logic_vector(31 downto 0);
      	Carryout : out std_logic;
      	Overflow : out std_logic;
      	Zero     : out std_logic);
end component;

component barrel_shifter
   port(input      : in std_logic_vector(31 downto 0);
      	shift      : in std_logic_vector(4 downto 0);
      	arithmetic : in std_logic;
      	shift_l    : in std_logic;
      	output     : out std_logic_vector(31 downto 0));
end component;

component structural_mux_n_bit
   generic(N : integer := 32);
   port(i0  : in std_logic_vector(N-1 downto 0);
      	i1  : in std_logic_vector(N-1 downto 0);
      	s   : in std_logic;
        o   : out std_logic_vector(N-1 downto 0));
end component;

signal alu_out, bs_out : std_logic_vector(31 downto 0);

begin

alu: alu_32b port map (A, B, Control, Unsign, alu_out, Carryout, Overflow, Zero);

bs: barrel_shifter port map (B, Shift_Amount, Arithmetic, Shift_Left, bs_out);

mux: structural_mux_n_bit port map (alu_out, bs_out, ALUorBS, Result);

end structure;
