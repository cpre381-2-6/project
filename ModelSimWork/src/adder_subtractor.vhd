library IEEE;
use IEEE.std_logic_1164.all;

entity adder_subtractor is
   generic(N : integer := 32);
   port(A        : in std_logic_vector(N-1 downto 0);
	B        : in std_logic_vector(N-1 downto 0);
	nAdd_Sub : in std_logic;
	S        : out std_logic_vector(N-1 downto 0);
        Cout     : out std_logic);

end adder_subtractor;

architecture structure of adder_subtractor is

component structural_oc
  generic(N : integer := 32);
  port(i_A  : in std_logic_vector(N-1 downto 0);
       o_F  : out std_logic_vector(N-1 downto 0));
end component;

component structural_mux_n_bit
   generic(N : integer := 32);
   port(i0  : in std_logic_vector(N-1 downto 0);
	i1  : in std_logic_vector(N-1 downto 0);
	s   : in std_logic;
        o   : out std_logic_vector(N-1 downto 0));
end component;

component structural_full_adder_n
   generic(N : integer := 32);
   port(A    : in std_logic_vector(N-1 downto 0);
	B    : in std_logic_vector(N-1 downto 0);
	Cin  : in std_logic;
	Sum  : out std_logic_vector(N-1 downto 0);
        Cout : out std_logic);
end component;

signal B_inv   : std_logic_vector(N-1 downto 0);
signal mux_val : std_logic_vector(N-1 downto 0);

begin

l0: structural_oc port map(B, B_inv);

l1: structural_mux_n_bit port map(B, B_inv, nAdd_Sub, mux_val);

l2: structural_full_adder_n port map(A, mux_val, nAdd_Sub, S, Cout);

end structure;