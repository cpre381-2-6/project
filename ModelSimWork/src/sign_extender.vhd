library IEEE;
use IEEE.std_logic_1164.all;

entity sign_extender is
   port(input  : in std_logic_vector(15 downto 0);
        output : out std_logic_vector(31 downto 0));

end sign_extender;

architecture dataflow of sign_extender is

begin

l0:  output(0)  <= input(0);
l1:  output(1)  <= input(1);
l2:  output(2)  <= input(2);
l3:  output(3)  <= input(3);
l4:  output(4)  <= input(4);
l5:  output(5)  <= input(5);
l6:  output(6)  <= input(6);
l7:  output(7)  <= input(7);
l8:  output(8)  <= input(8);
l9:  output(9)  <= input(9);
l10: output(10) <= input(10);
l11: output(11) <= input(11);
l12: output(12) <= input(12);
l13: output(13) <= input(13);
l14: output(14) <= input(14);
l15: output(15) <= input(15);

l16: output(16) <= input(15);
l17: output(17) <= input(15);
l18: output(18) <= input(15);
l19: output(19) <= input(15);
l20: output(20) <= input(15);
l21: output(21) <= input(15);
l22: output(22) <= input(15);
l23: output(23) <= input(15);
l24: output(24) <= input(15);
l25: output(25) <= input(15);
l26: output(26) <= input(15);
l27: output(27) <= input(15);
l28: output(28) <= input(15);
l29: output(29) <= input(15);
l30: output(30) <= input(15);
l31: output(31) <= input(15);

end dataflow;