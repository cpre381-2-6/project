library IEEE;
use IEEE.std_logic_1164.all;

entity IFID is
  port(clock         : in std_logic;
       flush         : in std_logic;
       stall         : in std_logic;
       i_instruction : in std_logic_vector(31 downto 0);
	   i_PcAddress   : in std_logic_vector(31 downto 0);
       o_instruction : out std_logic_vector(31 downto 0);
	   o_PcAddress   : out std_logic_vector(31 downto 0));
end IFID;

architecture structure of IFID is

signal not_stall : std_logic;

component n_bit_register
  generic(N : integer := 32);
  port(i_CLK        : in std_logic;
       i_RST        : in std_logic;
       i_WE         : in std_logic;
       i_D          : in std_logic_vector(N-1 downto 0);
       o_Q          : out std_logic_vector(N-1 downto 0));
end component;

begin

StallSignal: not_stall <= not stall;

InstructionRegister: n_bit_register
	generic map (N => 32)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_instruction,
		o_Q   => o_instruction);

PcAddressRegister: n_bit_register
	generic map (N => 32)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_PcAddress,
		o_Q   => o_PcAddress);

end structure;
