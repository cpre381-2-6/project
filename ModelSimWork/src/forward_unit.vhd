library IEEE;
use IEEE.std_logic_1164.all;

entity forward_unit is
   port(
	IdEx_op        : in std_logic_vector(5 downto 0);
	IdEx_func      : in std_logic_vector(5 downto 0);
	IdEx_s_RegWr   : in std_logic;
	IdEx_rd        : in std_logic_vector(4 downto 0);
	IdEx_rs        : in std_logic_vector(4 downto 0);
	IdEx_rt        : in std_logic_vector(4 downto 0);
	
	ExMem_op       : in std_logic_vector(5 downto 0);
	ExMem_func     : in std_logic_vector(5 downto 0);
    ExMem_s_RegWr  : in std_logic;
	ExMem_rd       : in std_logic_vector(4 downto 0);
	ExMem_rs       : in std_logic_vector(4 downto 0);
	ExMem_rt       : in std_logic_vector(4 downto 0);
	
	MemWb_op       : in std_logic_vector(5 downto 0);
	MemWb_func     : in std_logic_vector(5 downto 0);
	MemWb_s_RegWr  : in std_logic;
	MemWb_rd       : in std_logic_vector(4 downto 0);
	MemWb_rs       : in std_logic_vector(4 downto 0);
	MemWb_rt       : in std_logic_vector(4 downto 0);
	
    ForwardA       : out std_logic_vector(1 downto 0);
	ForwardB       : out std_logic_vector(1 downto 0);
	ForwardC       : out std_logic_vector(1 downto 0));
end forward_unit;

architecture structure of forward_unit is

begin

P_IF: process (IdEx_op, IdEx_func, IdEx_s_RegWr, IdEx_rs, IdEx_rt, ExMem_op, ExMem_s_RegWr, ExMem_rd, ExMem_rt, MemWb_op, MemWb_s_RegWr, MemWb_rd, MemWb_rt)

 	begin
	
	if ((ExMem_op = "000000") and (ExMem_s_RegWr = '1') and (ExMem_rd = IdEx_rs)) then
		ForwardA <= "01";
		
	elsif ((ExMem_op /= "000000") and (ExMem_s_RegWr = '1') and (ExMem_rt = IdEx_rs)) then
		ForwardA <= "01";
		
	elsif ((MemWb_op = "000000") and (MemWb_s_RegWr = '1') and (MemWb_rd = IdEx_rs)) then
		ForwardA <= "10";
		
	elsif ((MemWb_op /= "000000") and (MemWb_s_RegWr = '1') and (MemWb_rt = IdEx_rs)) then
		ForwardA <= "10";
		
	else 
		ForwardA <= "00";
		
	end if;	
	
	if ((IdEx_op = "000000")) then
	
		if ((ExMem_op = "000000") and (ExMem_s_RegWr = '1') and (ExMem_rd = IdEx_rt)) then
			ForwardB <= "01";
			
		elsif ((ExMem_op /= "000000") and (ExMem_s_RegWr = '1') and (ExMem_rt = IdEx_rt)) then
			ForwardB <= "01";
			
		elsif ((MemWb_op = "000000") and (MemWb_s_RegWr = '1') and (MemWb_rd = IdEx_rt)) then
			ForwardB <= "10";
			
		elsif ((MemWb_op /= "000000") and (MemWb_s_RegWr = '1') and (MemWb_rt = IdEx_rt)) then
			ForwardB <= "10";
			
		else 
			ForwardB <= "00";
			
		end if;
		
	else
		ForwardB <= "00";
		
	end if;	
	
	if ((ExMem_s_RegWr = '1') and (ExMem_rd = IdEx_rt) and (ExMem_op = "000000")) then
		ForwardC <= "01";
		
	elsif ((ExMem_s_RegWr = '1') and (ExMem_rt = IdEx_rt)) then
		ForwardC <= "01";
		
	elsif ((MemWb_s_RegWr = '1') and (MemWb_rd = IdEx_rt) and (MemWb_op = "000000")) then
		ForwardC <= "10";
		
	elsif ((MemWb_s_RegWr = '1') and (MemWb_rt = IdEx_rt)) then
		ForwardC <= "10";
		
	else 
		ForwardC <= "00";
		
	end if;

end process;

end structure;