library IEEE;
use IEEE.std_logic_1164.all;

entity tb_register_file is
  generic(gCLK_HPER : time := 50 ns;
	  N         : integer := 32);
end tb_register_file;

architecture behavior of tb_register_file is
  
  -- Calculate the clock period as twice the half-period
  constant cCLK_PER  : time := gCLK_HPER * 2;


  component register_file
  port( clock         : in STD_LOGIC;
	reset         : in STD_LOGIC;
	d0            : out STD_LOGIC_VECTOR(31 downto 0);
	d1            : out STD_LOGIC_VECTOR(31 downto 0);
	r0            : in STD_LOGIC_VECTOR(4 downto 0);
	r1            : in STD_LOGIC_VECTOR(4 downto 0);
	register_data : in STD_LOGIC_VECTOR(31 downto 0);
	register_num  : in STD_LOGIC_VECTOR(4 downto 0);
        write_enable  : in STD_LOGIC);
  end component;

  -- Temporary signals to connect to the dff component.
  signal s_CLK, s_RST, s_WE   : std_logic;
  signal d0, d1, register_data : std_logic_vector(31 downto 0);
  signal r0, r1, register_num : std_logic_vector(4 downto 0);

begin

  DUT: register_file port map(s_CLK, s_RST, d0, d1, r0, r1, register_data, register_num, s_WE);

  -- This process sets the clock value (low for gCLK_HPER, then high
  -- for gCLK_HPER). Absent a "wait" command, processes restart 
  -- at the beginning once they have reached the final statement.
  P_CLK: process
  begin
    s_CLK <= '0';
    wait for gCLK_HPER;
    s_CLK <= '1';
    wait for gCLK_HPER;
  end process;
  
  -- Testbench process  
  P_TB: process
  begin

    s_RST <= '1';
    s_WE  <= '0';
    wait for cCLK_PER;

    s_RST <= '0';
    wait for cCLK_PER;

    s_WE  <= '1';
    register_data <= x"00000005";
    register_num  <= "00001";
    wait for cCLK_PER;

    r0 <= "00000";
    r1 <= "00001";
    wait for cCLK_PER;

    register_data <= x"00000010";
    register_num  <= "00010";
    wait for cCLK_PER;

    r0 <= "00010";
    wait for cCLK_PER;

    r0 <= "00001";
    r1 <= "00010";
    wait for cCLK_PER;

    s_RST <= '1';
    wait for cCLK_PER;

    r0 <= "00010";
    r1 <= "00001";
    wait for cCLK_PER;

  end process;
  
end behavior;