library IEEE;
use IEEE.std_logic_1164.all;

entity tb_pipeline_reg is
  generic(gCLK_HPER : time := 50 ns;
	  N         : integer := 32);
end tb_pipeline_reg;

architecture behavior of tb_pipeline_reg is

  constant cCLK_PER  : time := gCLK_HPER * 2;


  component IFID
    port(clock         : in std_logic;
         flush         : in std_logic;
         stall         : in std_logic;
         i_instruction : in std_logic_vector(31 downto 0);
  	     i_PcAddress   : in std_logic_vector(31 downto 0);
         o_instruction : out std_logic_vector(31 downto 0);
  	     o_PcAddress   : out std_logic_vector(31 downto 0));
  end component;

  component IDEX
    port(	clock        : in std_logic;
  		flush        : in std_logic;
  		stall        : in std_logic;

  		i_ALUControl : in STD_LOGIC_VECTOR(2 downto 0);
  		i_ALUorBS    : in STD_LOGIC;
  		i_ALUSrc     : in STD_LOGIC;
  		i_Arithmetic : in STD_LOGIC;

  		i_JAL        : in STD_LOGIC;

  		i_MemToReg   : in STD_LOGIC;
  		i_RegDst     : in STD_LOGIC;
  		i_s_DMemWr   : in STD_LOGIC;
  		i_s_RegWr    : in STD_LOGIC;
  		i_Shift_L    : in STD_LOGIC;
  		i_Shift_Sel  : in STD_LOGIC_VECTOR(1 downto 0);
  		i_Unsign     : in STD_LOGIC;

  		i_PcAddress  : in std_logic_vector(31 downto 0);
  		i_d0		     : in std_logic_vector(31 downto 0);
  		i_d1		     : in std_logic_vector(31 downto 0);
  		i_extended   : in std_logic_vector(31 downto 0);
  		i_rs         : in std_logic_vector(4 downto 0);
  		i_rt         : in std_logic_vector(4 downto 0);
  		i_rd         : in std_logic_vector(4 downto 0);
  		i_shamt      : in std_logic_vector(4 downto 0);

  		o_ALUControl : out STD_LOGIC_VECTOR(2 downto 0);
  		o_ALUorBS    : out STD_LOGIC;
  		o_ALUSrc     : out STD_LOGIC;
  		o_Arithmetic : out STD_LOGIC;

  		o_JAL        : out STD_LOGIC;

  		o_MemToReg   : out STD_LOGIC;
  		o_RegDst     : out STD_LOGIC;
  		o_s_DMemWr   : out STD_LOGIC;
  		o_s_RegWr    : out STD_LOGIC;
  		o_Shift_L    : out STD_LOGIC;
  		o_Shift_Sel  : out STD_LOGIC_VECTOR(1 downto 0);
  		o_Unsign     : out STD_LOGIC;

  		o_PcAddress  : out std_logic_vector(31 downto 0);
  		o_d0         : out std_logic_vector(31 downto 0);
  		o_d1		 : out std_logic_vector(31 downto 0);
  		o_extended   : out std_logic_vector(31 downto 0);
  		o_rs         : out std_logic_vector(4 downto 0);
  		o_rt         : out std_logic_vector(4 downto 0);
  		o_rd         : out std_logic_vector(4 downto 0);
  		o_shamt      : out std_logic_vector(4 downto 0));
  end component;

  component EXMEM
    port(	
		clock         : in std_logic;
  		flush         : in std_logic;
  		stall         : in std_logic;

  		i_JAL         : in STD_LOGIC;
		
  		i_MemToReg    : in STD_LOGIC;
  		i_s_DMemWr    : in STD_LOGIC;
  		i_s_RegWr     : in STD_LOGIC;

  		i_ALUResult   : in std_logic_vector(31 downto 0);
  		i_d1          : in std_logic_vector(31 downto 0);
  		i_destination : in std_logic_vector(4 downto 0);

  		i_rs          : in std_logic_vector(4 downto 0);
  		i_rt          : in std_logic_vector(4 downto 0);
  		i_rd          : in std_logic_vector(4 downto 0);

  		o_JAL         : out STD_LOGIC;

  		o_MemToReg    : out STD_LOGIC;
  		o_s_DMemWr    : out STD_LOGIC;
  		o_s_RegWr     : out STD_LOGIC;

  		o_ALUResult   : out std_logic_vector(31 downto 0);
  		o_d1          : out std_logic_vector(31 downto 0);
  		o_destination : out std_logic_vector(4 downto 0);

  		o_rs          : out std_logic_vector(4 downto 0);
  		o_rt          : out std_logic_vector(4 downto 0);
  		o_rd          : out std_logic_vector(4 downto 0));
  end component;

  component MEMWB
    port(	clock         : in std_logic;
  		flush         : in std_logic;
  		stall         : in std_logic;

  		i_MemToReg    : in STD_LOGIC;
  		i_s_RegWr     : in STD_LOGIC;
  		i_ALUResult   : in std_logic_vector(31 downto 0);
  		i_destination : in std_logic_vector(4 downto 0);
  		i_rs          : in std_logic_vector(4 downto 0);
  		i_rt          : in std_logic_vector(4 downto 0);
  		i_rd          : in std_logic_vector(4 downto 0);

  		o_MemToReg    : out STD_LOGIC;
  		o_s_RegWr     : out STD_LOGIC;
  		o_ALUResult   : out std_logic_vector(31 downto 0);
  		o_destination : out std_logic_vector(4 downto 0);
  		o_rs          : out std_logic_vector(4 downto 0);
  		o_rt          : out std_logic_vector(4 downto 0);
  		o_rd          : out std_logic_vector(4 downto 0));
  end component;

  signal s_CLK : std_logic;
  
  --IFID
  signal IfId_FLUSH, IfId_STALL : std_logic;
  signal instruction, PcAddress, IfIdInstruction, IfIdPcAddress : std_logic_vector(31 downto 0);

  --IDEX
  signal IdEx_FLUSH, IdEx_STALL : std_logic;
  signal ALUorBS, ALUSrc, Arithmetic, BNE, Branch, J, JAL, JR, MemToReg, RegDst, s_DMemWr, s_RegWr, Shift_L, Unsign : std_logic; 
  signal IdEx_ALUorBS, IdEx_ALUSrc, IdEx_Arithmetic, IdEx_BNE, IdEx_Branch, IdEx_J, IdEx_JAL, IdEx_JR, IdEx_MemToReg, IdEx_RegDst, IdEx_s_DMemWr, IdEx_s_RegWr, IdEx_Shift_L, IdEx_Unsign : std_logic; 
  signal ALUControl, IdEx_ALUControl : std_logic_vector(2 downto 0);
  signal Shift_Sel, IdEx_Shift_Sel : std_logic_vector(1 downto 0);
  signal d0, d1 : std_logic_vector(31 downto 0);
  signal IdEx_PcAddress, IdEx_d0, IdEx_d1, IdEx_extended : std_logic_vector(31 downto 0);
  signal IdEx_rs, IdEx_rt, IdEx_rd, IdEx_shamt : std_logic_vector(4 downto 0);

  --EXMEM
  signal ExMem_FLUSH, ExMem_STALL, zero : std_logic;
  signal ALUResult : std_logic_vector(31 downto 0);
  signal destination : std_logic_vector(4 downto 0);
  signal ExMem_BNE, ExMem_Branch, ExMem_J, ExMem_JAL, ExMem_JR, ExMem_MemToReg, ExMem_s_DMemWr, ExMem_s_RegWr, ExMem_zero : std_logic;
  signal ExMem_PcAddress, ExMem_ALUResult, ExMem_d1 : std_logic_vector(31 downto 0);
  signal ExMem_destination, ExMem_rs, ExMem_rt, ExMem_rd : std_logic_vector(4 downto 0);

  --MEMWB
  signal MemWb_FLUSH, MemWb_STALL, MemWb_MemToReg, MemWb_s_RegWr : std_logic;
  signal MemWb_ALUResult : std_logic_vector(31 downto 0);
  signal MemWb_destination, MemWb_rs, MemWb_rt, MemWb_rd : std_logic_vector(4 downto 0);

begin

	IFIDRegister: IFID port map(
		clock         => s_CLK,
        flush         => IfId_FLUSH,
        stall         => IfId_STALL,
        i_instruction => instruction,
  	    i_PcAddress   => PcAddress,
        o_instruction => IfIdInstruction,
  	    o_PcAddress   => IfIdPcAddress);
		
	IDEXRegister: IDEX port map(
		clock        => s_CLK,
  		flush        => IdEx_FLUSH,
  		stall        => IdEx_STALL,

  		i_ALUControl => ALUControl,
  		i_ALUorBS    => ALUorBS,
  		i_ALUSrc     => ALUSrc,
  		i_Arithmetic => Arithmetic,

  		i_JAL        => JAL,

  		i_MemToReg   => MemToReg,
  		i_RegDst     => RegDst,
  		i_s_DMemWr   => s_DMemWr,
  		i_s_RegWr    => s_RegWr,
  		i_Shift_L    => Shift_L ,
  		i_Shift_Sel  => Shift_Sel, 
  		i_Unsign     => Unsign,

  		i_PcAddress  => IfIdPcAddress,
  		i_d0		 => d0,
  		i_d1		 => d1,
  		i_extended   => x"00000000",
  		i_rs         => IfIdInstruction(25 downto 21),
  		i_rt         => IfIdInstruction(20 downto 16),
  		i_rd         => IfIdInstruction(15 downto 11),
  		i_shamt      => IfIdInstruction(10 downto 6),

  		o_ALUControl => IdEx_ALUControl,
  		o_ALUorBS    => IdEx_ALUorBS,
  		o_ALUSrc     => IdEx_ALUSrc,
  		o_Arithmetic => IdEx_Arithmetic,

  		o_JAL        => IdEx_JAL,
		
  		o_MemToReg   => IdEx_MemToReg,
  		o_RegDst     => IdEx_RegDst,
  		o_s_DMemWr   => IdEx_s_DMemWr,
  		o_s_RegWr    => IdEx_s_RegWr,
  		o_Shift_L    => IdEx_Shift_L ,
  		o_Shift_Sel  => IdEx_Shift_Sel, 
  		o_Unsign     => IdEx_Unsign,

  		o_PcAddress  => IdEx_PcAddress,
  		o_d0         => IdEx_d0,
  		o_d1		 => IdEx_d1,
  		o_extended   => IdEx_extended,
  		o_rs         => IdEx_rs,
  		o_rt         => IdEx_rt,
  		o_rd         => IdEx_rd,
  		o_shamt      => IdEx_shamt);

	EXMEMRegister: EXMEM port map(
		clock         => s_CLK,
  		flush         => ExMem_FLUSH,
  		stall         => ExMem_STALL,

  		i_JAL         => IdEx_JAL,

  		i_MemToReg    => IdEx_MemToReg,
  		i_s_DMemWr    => IdEx_s_DMemWr,
  		i_s_RegWr     => IdEx_s_RegWr,

  		i_ALUResult   => ALUResult,
  		i_d1          => IdEx_d1,
  		i_destination => destination,

  		i_rs          => IdEx_rs,
  		i_rt          => IdEx_rt,
  		i_rd          => IdEx_rd,

  		o_JAL         => ExMem_JAL,

  		o_MemToReg    => ExMem_MemToReg,
  		o_s_DMemWr    => ExMem_s_DMemWr,
  		o_s_RegWr     => ExMem_s_RegWr,

  		o_ALUResult   => ExMem_ALUResult,
  		o_d1          => ExMem_d1,
  		o_destination => ExMem_destination,

  		o_rs          => ExMem_rs,
  		o_rt          => ExMem_rt,
  		o_rd          => ExMem_rd);
		
	MEMWBRegister: MEMWB port map(
        clock         => s_CLK,
		flush         => MemWb_FLUSH,
		stall         => MemWb_STALL,
		
		i_MemToReg    => ExMem_MemToReg,
		i_s_RegWr     => ExMem_s_RegWr,
		i_ALUResult   => ExMem_ALUResult,
		i_destination => ExMem_destination,
		i_rs          => ExMem_rs,
		i_rt          => ExMem_rt,
		i_rd          => ExMem_rd,		
		
		o_MemToReg    => MemWb_MemToReg,
		o_s_RegWr     => MemWb_s_RegWr,
		o_ALUResult   => MemWb_ALUResult,
		o_destination => MemWb_destination,
		o_rs          => MemWb_rs,
		o_rt          => MemWb_rt,
		o_rd          => MemWb_rd);

  P_CLK: process
  begin
    s_CLK <= '0';
    wait for gCLK_HPER;
    s_CLK <= '1';
    wait for gCLK_HPER;
  end process;

  -- Testbench process
  P_TB: process
  begin

	IfId_FLUSH  <= '0';
	IfId_STALL  <= '0';
	
	IdEx_FLUSH  <= '0';
	IdEx_STALL  <= '0';
	
	ExMem_FLUSH <= '0';
	ExMem_STALL <= '0';
	
	MemWb_FLUSH <= '0';
	MemWB_STALL <= '0';
	
	instruction <= x"00000000";
	PcAddress   <= x"00000001";
	
    	wait for cCLK_PER;
	
	IfId_FLUSH  <= '0';
	IfId_STALL  <= '0';
	
	IdEx_FLUSH  <= '0';
	IdEx_STALL  <= '0';
	
	ExMem_FLUSH <= '0';
	ExMem_STALL <= '0';
	
	MemWb_FLUSH <= '0';
	MemWB_STALL <= '0';
	
	instruction <= x"11111111";
	PcAddress   <= x"00000002";

	wait for cCLK_PER;

	IfId_FLUSH  <= '0';
	IfId_STALL  <= '1';
	
	IdEx_FLUSH  <= '0';
	IdEx_STALL  <= '0';
	
	ExMem_FLUSH <= '0';
	ExMem_STALL <= '0';
	
	MemWb_FLUSH <= '0';
	MemWB_STALL <= '0';
	
	instruction <= x"22222222";
	PcAddress   <= x"00000003";

	wait for cCLK_PER;

	IfId_FLUSH  <= '0';
	IfId_STALL  <= '0';
	
	IdEx_FLUSH  <= '0';
	IdEx_STALL  <= '0';
	
	ExMem_FLUSH <= '0';
	ExMem_STALL <= '0';
	
	MemWb_FLUSH <= '0';
	MemWB_STALL <= '0';
	
	instruction <= x"33333333";
	PcAddress   <= x"00000004";

	wait for cCLK_PER;

	IfId_FLUSH  <= '0';
	IfId_STALL  <= '0';
	
	IdEx_FLUSH  <= '0';
	IdEx_STALL  <= '0';
	
	ExMem_FLUSH <= '1';
	ExMem_STALL <= '0';
	
	MemWb_FLUSH <= '0';
	MemWB_STALL <= '0';
	
	instruction <= x"44444444";
	PcAddress   <= x"00000005";

	wait for cCLK_PER;
	
	IfId_FLUSH  <= '0';
	IfId_STALL  <= '0';
	
	IdEx_FLUSH  <= '0';
	IdEx_STALL  <= '1';
	
	ExMem_FLUSH <= '0';
	ExMem_STALL <= '0';
	
	MemWb_FLUSH <= '0';
	MemWB_STALL <= '1';
	
	instruction <= x"55555555";
	PcAddress   <= x"00000006";

	wait for cCLK_PER;
	
	IfId_FLUSH  <= '0';
	IfId_STALL  <= '0';
	
	IdEx_FLUSH  <= '0';
	IdEx_STALL  <= '0';
	
	ExMem_FLUSH <= '0';
	ExMem_STALL <= '0';
	
	MemWb_FLUSH <= '0';
	MemWB_STALL <= '0';
	
	instruction <= x"66666666";
	PcAddress   <= x"00000007";

	wait for cCLK_PER;
	
	IfId_FLUSH  <= '0';
	IfId_STALL  <= '0';
	
	IdEx_FLUSH  <= '1';
	IdEx_STALL  <= '0';
	
	ExMem_FLUSH <= '0';
	ExMem_STALL <= '1';
	
	MemWb_FLUSH <= '0';
	MemWB_STALL <= '0';
	
	instruction <= x"77777777";
	PcAddress   <= x"00000008";

	wait for cCLK_PER;
	
	IfId_FLUSH  <= '1';
	IfId_STALL  <= '0';
	
	IdEx_FLUSH  <= '0';
	IdEx_STALL  <= '0';
	
	ExMem_FLUSH <= '0';
	ExMem_STALL <= '0';
	
	MemWb_FLUSH <= '0';
	MemWB_STALL <= '0';
	
	instruction <= x"88888888";
	PcAddress   <= x"00000009";

	wait for cCLK_PER;
	
	IfId_FLUSH  <= '0';
	IfId_STALL  <= '0';
	
	IdEx_FLUSH  <= '0';
	IdEx_STALL  <= '0';
	
	ExMem_FLUSH <= '0';
	ExMem_STALL <= '0';
	
	MemWb_FLUSH <= '0';
	MemWB_STALL <= '0';
	
	instruction <= x"99999999";
	PcAddress   <= x"0000000A";

	wait for cCLK_PER;
	
	IfId_FLUSH  <= '0';
	IfId_STALL  <= '0';
	
	IdEx_FLUSH  <= '0';
	IdEx_STALL  <= '0';
	
	ExMem_FLUSH <= '0';
	ExMem_STALL <= '0';
	
	MemWb_FLUSH <= '1';
	MemWB_STALL <= '0';
	
	instruction <= x"AAAAAAAA";
	PcAddress   <= x"0000000B";

	wait for cCLK_PER;

  end process;

end behavior;
