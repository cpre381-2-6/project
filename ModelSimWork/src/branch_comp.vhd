library IEEE;
use IEEE.std_logic_1164.all;

entity branch_comp is
   port(
    A        : in std_logic_vector(31 downto 0);
	B        : in std_logic_vector(31 downto 0);
    S        : out std_logic);
end branch_comp;

architecture structure of branch_comp is

signal s_Carryout, s_Overflow  : std_logic;
signal s_Result : std_logic_vector(31 downto 0);

begin

process (A, B)

	begin
		
	if A = B then
		S <= '1';
	else
		S <= '0';
	end if;

end process;

end structure;