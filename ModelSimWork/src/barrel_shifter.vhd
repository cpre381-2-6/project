library IEEE;
use IEEE.std_logic_1164.all;

entity barrel_shifter is
   port(input      : in std_logic_vector(31 downto 0);
	shift      : in std_logic_vector(4 downto 0);
	arithmetic : in std_logic;
	shift_l    : in std_logic;
	output     : out std_logic_vector(31 downto 0));
end barrel_shifter;

architecture structure of barrel_shifter is

component mux_2
    port ( sel  : in  STD_LOGIC;
           i0   : in  STD_LOGIC;
           i1   : in  STD_LOGIC;
           o    : out STD_LOGIC);
end component;

signal new_bit                           : std_logic;

signal col, col_0, col_1, col_2, col_3, col_4 : std_logic_vector(31 downto 0);

begin

-- INPUT

process (input, shift, shift_l)
	begin
	if shift_l = '0' then
		col <= input;
	else
		col(0)  <= input(31);
		col(1)  <= input(30);
		col(2)  <= input(29);
		col(3)  <= input(28);
		col(4)  <= input(27);
		col(5)  <= input(26);
		col(6)  <= input(25);
		col(7)  <= input(24);
		col(8)  <= input(23);
		col(9)  <= input(22);
		col(10) <= input(21);
		col(11) <= input(20);
		col(12) <= input(19);
		col(13) <= input(18);
		col(14) <= input(17);
		col(15) <= input(16);
		col(16) <= input(15);
		col(17) <= input(14);
		col(18) <= input(13);
		col(19) <= input(12);
		col(20) <= input(11);
		col(21) <= input(10);
		col(22) <= input(9);
		col(23) <= input(8);
		col(24) <= input(7);
		col(25) <= input(6);
		col(26) <= input(5);
		col(27) <= input(4);
		col(28) <= input(3);
		col(29) <= input(2);
		col(30) <= input(1);
		col(31) <= input(0);	
	end if;
end process;

newBit: mux_2 port map (arithmetic, '0', col(31), new_bit);

-- COLUMN 0

col_0_muxes: for i in 0 to 30 generate
  col_0_i: mux_2 port map(shift(0), col(i), col(i + 1), col_0(i));
end generate;

col_0_mux_31: mux_2 port map (shift(0), col(31), new_bit, col_0(31));

-- COLUMN 1

col_1_muxes: for i in 0 to 29 generate
  col_1_i: mux_2 port map(shift(1), col_0(i), col_0(i + 2), col_1(i));
end generate;

col_1_mux_31: mux_2 port map (shift(1), col_0(31), new_bit, col_1(31));

col_1_mux_30: mux_2 port map (shift(1), col_0(30), new_bit, col_1(30));

-- COLUMN 2

col_2_muxes_0: for i in 0 to 27 generate
  col_2_i_0: mux_2 port map(shift(2), col_1(i), col_1(i + 4), col_2(i));
end generate;

col_2_muxes_1: for i in 28 to 31 generate
  col_2_i_1: mux_2 port map (shift(2), col_1(i), new_bit, col_2(i));
end generate;

-- COLUMN 3

col_3_muxes_0: for i in 0 to 23 generate
  col_3_i_0: mux_2 port map(shift(3), col_2(i), col_2(i + 8), col_3(i));
end generate;

col_3_muxes_1: for i in 24 to 31 generate
  col_3_i_1: mux_2 port map (shift(3), col_2(i), new_bit, col_3(i));
end generate;

-- COLUMN 4

col_4_muxes_0: for i in 0 to 15 generate
  col_4_i_0: mux_2 port map(shift(4), col_3(i), col_3(i + 16), col_4(i));
end generate;

col_4_muxes_1: for i in 16 to 31 generate
  col_4_i_1: mux_2 port map (shift(4), col_3(i), new_bit, col_4(i));
end generate;

-- OUTPUT

process (col_4, shift, shift_l)
	begin
	if shift_l = '0' then
		output <= col_4;
	else
		output(0)  <= col_4(31);
		output(1)  <= col_4(30);
		output(2)  <= col_4(29);
		output(3)  <= col_4(28);
		output(4)  <= col_4(27);
		output(5)  <= col_4(26);
		output(6)  <= col_4(25);
		output(7)  <= col_4(24);
		output(8)  <= col_4(23);
		output(9)  <= col_4(22);
		output(10) <= col_4(21);
		output(11) <= col_4(20);
		output(12) <= col_4(19);
		output(13) <= col_4(18);
		output(14) <= col_4(17);
		output(15) <= col_4(16);
		output(16) <= col_4(15);
		output(17) <= col_4(14);
		output(18) <= col_4(13);
		output(19) <= col_4(12);
		output(20) <= col_4(11);
		output(21) <= col_4(10);
		output(22) <= col_4(9);
		output(23) <= col_4(8);
		output(24) <= col_4(7);
		output(25) <= col_4(6);
		output(26) <= col_4(5);
		output(27) <= col_4(4);
		output(28) <= col_4(3);
		output(29) <= col_4(2);
		output(30) <= col_4(1);
		output(31) <= col_4(0);	
	end if;
end process;

end structure;