library IEEE;
use IEEE.std_logic_1164.all;

entity n_bit_register is
  generic(N : integer := 32);
  port(i_CLK        : in std_logic;
       i_RST        : in std_logic;
       i_WE         : in std_logic;
       i_D          : in std_logic_vector(N-1 downto 0);
       o_Q          : out std_logic_vector(N-1 downto 0));
end n_bit_register;

architecture structure of n_bit_register is

component dflipf
  port(i_CLK        : in std_logic;
       i_RST        : in std_logic;
       i_WE         : in std_logic;
       i_D          : in std_logic;
       o_Q          : out std_logic);
end component;

begin

l0: for i in 0 to N-1 generate
  dff_i: dflipf port map(i_CLK, i_RST, i_WE, i_D(i), o_Q(i));
end generate;

end structure;