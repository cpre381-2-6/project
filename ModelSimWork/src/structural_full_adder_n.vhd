library IEEE;
use IEEE.std_logic_1164.all;

entity structural_full_adder_n is
   generic(N : integer := 32);
   port(A    : in std_logic_vector(N-1 downto 0);
	B    : in std_logic_vector(N-1 downto 0);
	Cin  : in std_logic;
	Sum  : out std_logic_vector(N-1 downto 0);
        Cout : out std_logic);

end structural_full_adder_n;

architecture structure of structural_full_adder_n is

component structural_full_adder
   port(A    : in std_logic;
	B    : in std_logic;
	Cin  : in std_logic;
	Sum  : out std_logic;
        Cout : out std_logic);
end component;

signal carry : std_logic_vector(N-1 downto 0);

begin

l0: structural_full_adder port map(A(0), B(0), Cin, Sum(0), carry(0));

l1: for i in 1 to N-1 generate
  structural_full_adder_i: structural_full_adder port map(A(i), B(i), carry(i-1), Sum(i), carry(i));
end generate;

l2: Cout <= carry(N-1);

end structure;