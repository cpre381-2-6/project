library IEEE;
use IEEE.std_logic_1164.all;

entity processor is
  port( clock         : in STD_LOGIC;
	reset         : in STD_LOGIC;
	rs            : in STD_LOGIC_VECTOR(4 downto 0);
	rt            : in STD_LOGIC_VECTOR(4 downto 0);
        regWrite      : in STD_LOGIC;
        regAddress    : in STD_LOGIC_VECTOR(4 downto 0);
	immediate     : in STD_LOGIC_VECTOR(15 downto 0);
	Control       : in std_logic_vector(2 downto 0);
	Arithmetic    : in std_logic;
	shift_l       : in std_logic;
	ALUorBS       : in std_logic;
        ALUSrc        : in STD_LOGIC;
	memWrite      : in STD_LOGIC;
	memToReg      : in STD_LOGIC;
        reg0          : out STD_LOGIC_VECTOR(31 downto 0);
        reg1          : out STD_LOGIC_VECTOR(31 downto 0);
        ASout         : out STD_LOGIC_VECTOR(31 downto 0);
	memOut        : out STD_LOGIC_VECTOR(31 downto 0);
	Cout          : out std_logic;
	Overflow      : out std_logic; 
	Zero 	      : out std_logic);
end processor;

architecture structure of processor is

component register_file
  port( clock         : in STD_LOGIC;
	reset         : in STD_LOGIC;
	d0            : out STD_LOGIC_VECTOR(31 downto 0);
	d1            : out STD_LOGIC_VECTOR(31 downto 0);
	r0            : in STD_LOGIC_VECTOR(4 downto 0);
	r1            : in STD_LOGIC_VECTOR(4 downto 0);
	register_data : in STD_LOGIC_VECTOR(31 downto 0);
	register_num  : in STD_LOGIC_VECTOR(4 downto 0);
        write_enable  : in STD_LOGIC);
end component;

component structural_mux_n_bit
   generic(N : integer := 32);
   port(i0  : in std_logic_vector(N-1 downto 0);
	i1  : in std_logic_vector(N-1 downto 0);
	s   : in std_logic;
        o   : out std_logic_vector(N-1 downto 0));
end component;

component ALU
   port(A          : in std_logic_vector(31 downto 0);
	B          : in std_logic_vector(31 downto 0);
	Control    : in std_logic_vector(2 downto 0);
	Arithmetic : in std_logic;
	shift_l    : in std_logic;
	ALUorBS    : in std_logic;
	Result     : out std_logic_vector(31 downto 0);
	Carryout   : out std_logic;
	Overflow   : out std_logic;
	Zero       : out std_logic);
end component;

component sign_extender 
   port(input  : in std_logic_vector(15 downto 0);
        output : out std_logic_vector(31 downto 0));
end component;

component mem
	generic (DATA_WIDTH : natural := 32;
	         ADDR_WIDTH : natural := 10);
	port(clk		: in std_logic;
	     addr	        : in std_logic_vector((ADDR_WIDTH-1) downto 0);
	     data	        : in std_logic_vector((DATA_WIDTH-1) downto 0);
	     we		: in std_logic := '1';
	     q		: out std_logic_vector((DATA_WIDTH -1) downto 0));
end component;

signal S, d0, d1, mux1Out, mux2Out, extenderOut, Q : STD_LOGIC_VECTOR(31 downto 0);

begin

line0: register_file port map (clock, reset, d0, d1, rs, rt, mux2OUT, regAddress, regWrite);

line1: sign_extender port map (immediate, extenderOut); 

line2: structural_mux_n_bit port map (d1, extenderOut, ALUSrc, mux1Out);

line3: ALU port map (d0, mux1Out, Control, Arithmetic, shift_l, ALUorBS, S, Cout, Overflow, Zero);

line4: mem port map (clock, S(11 downto 2), d1, memWrite, Q);

line5: memOut <= Q;

line6: structural_mux_n_bit port map (S, Q, memToReg, mux2Out);

line7: reg0 <= d0;

line8: reg1 <= d1;

line9: ASout <= S;

end structure;