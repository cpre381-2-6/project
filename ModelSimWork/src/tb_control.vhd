library IEEE;
use IEEE.std_logic_1164.all;

entity tb_control is

end tb_control;

architecture behavior of tb_control is

component control
    port(func : in STD_LOGIC_VECTOR(5 downto 0);
  	     op   : in STD_LOGIC_VECTOR(5 downto 0);
  	     ALUControl : out STD_LOGIC_VECTOR(2 downto 0);
  	     ALUorBS    : out STD_LOGIC;
  	     ALUSrc     : out STD_LOGIC;
  	     Arithmetic : out STD_LOGIC;
	     BNE        : out STD_LOGIC;
	     J          : out STD_LOGIC;
	     JAL        : out STD_LOGIC;
             JR         : out STD_LOGIC;
  	     MemToReg   : out STD_LOGIC;
  	     RegDst     : out STD_LOGIC;
  	     s_DMemWr   : out STD_LOGIC;
  	     s_RegWr    : out STD_LOGIC;
  	     Shift_L    : out STD_LOGIC;
  	     Shift_Sel  : out STD_LOGIC_VECTOR(1 downto 0);
  	     Unsign     : out STD_LOGIC);
end component;

signal ALUorBS, ALUSrc, Arithmetic, BNE, J, JAL, JR, MemToReg, RegDst, s_DMemWr, s_RegWr, Shift_L, Unsign : std_logic;
signal Shift_Sel : std_logic_vector(1 downto 0);
signal ALUControl : std_logic_vector(2 downto 0);
signal func, op : std_logic_vector(5 downto 0);

begin

DUT0: control port map(func, op, ALUControl, ALUorBS, ALUSrc, Arithmetic, BNE, J, JAL, JR, MemToReg, RegDst, s_DMemWr, s_RegWr, Shift_L, Shift_Sel, Unsign);

  process
  begin

	--addi
	func <= "000000";
	op   <= "001000";
    	wait for 100 ns;

	--add
	func <= "100000";
	op   <= "000000";
    	wait for 100 ns;

	--addiu 
	func <= "000000";
	op   <= "001001";
    	wait for 100 ns;

	--addu 
	func <= "100001";
	op   <= "000000";
    	wait for 100 ns;

	--and
	func <= "100100";
	op   <= "000000";
    	wait for 100 ns;

	--andi 
	func <= "000000";
	op   <= "001100";
    	wait for 100 ns;

	--lui 
	func <= "000000";
	op   <= "001111";
    	wait for 100 ns;

	--lw
	func <= "000000";
	op   <= "010111";
    	wait for 100 ns;

	--nor 
	func <= "100111";
	op   <= "000000";
    	wait for 100 ns;

	--xor 
	func <= "100110";
	op   <= "000000";
    	wait for 100 ns;

	--xori 
	func <= "000000";
	op   <= "001110";
    	wait for 100 ns;

	--or 
	func <= "100101";
	op   <= "000000";
    	wait for 100 ns;

	--ori
	func <= "000000";
	op   <= "001101";
    	wait for 100 ns;

	--slt
	func <= "101010";
	op   <= "000000";
    	wait for 100 ns;

	--slti
	func <= "000000";
	op   <= "001010";
    	wait for 100 ns;

	--sltiu
	func <= "000000";
	op   <= "001011";
    	wait for 100 ns;

	--sltu
	func <= "101011";
	op   <= "000000";
    	wait for 100 ns;

	--sll
	func <= "000000";
	op   <= "000000";
    	wait for 100 ns;

	--srl
	func <= "000010";
	op   <= "000000";
    	wait for 100 ns;

	--sra
	func <= "000011";
	op   <= "000000";
    	wait for 100 ns;

	--sllv
	func <= "000100";
	op   <= "000000";
    	wait for 100 ns;

	--srlv
	func <= "000110";
	op   <= "000000";
    	wait for 100 ns;

	--srav
	func <= "000111";
	op   <= "000000";
    	wait for 100 ns;

	--sw
	func <= "000000";
	op   <= "101011";
    	wait for 100 ns;

	--sub
	func <= "100010";
	op   <= "000000";
    	wait for 100 ns;

	--subu
	func <= "100011";
	op   <= "000000";
    	wait for 100 ns;

  end process;

end behavior;