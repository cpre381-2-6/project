library IEEE;
use IEEE.std_logic_1164.all;

entity tb_processor is
  generic(gCLK_HPER : time := 50 ns;
	  N         : integer := 32);
end tb_processor;

architecture behavior of tb_processor is
  
  -- Calculate the clock period as twice the half-period
  constant cCLK_PER  : time := gCLK_HPER * 2;


  component processor
  port( clock         : in STD_LOGIC;
	reset         : in STD_LOGIC;
	rs            : in STD_LOGIC_VECTOR(4 downto 0);
	rt            : in STD_LOGIC_VECTOR(4 downto 0);
        regWrite      : in STD_LOGIC;
        regAddress    : in STD_LOGIC_VECTOR(4 downto 0);
	immediate     : in STD_LOGIC_VECTOR(15 downto 0);
	Control       : in std_logic_vector(2 downto 0);
	Arithmetic    : in std_logic;
	shift_l       : in std_logic;
	ALUorBS       : in std_logic;
        ALUSrc        : in STD_LOGIC;
	memWrite      : in STD_LOGIC;
	memToReg      : in STD_LOGIC;
        reg0          : out STD_LOGIC_VECTOR(31 downto 0);
        reg1          : out STD_LOGIC_VECTOR(31 downto 0);
        ASout         : out STD_LOGIC_VECTOR(31 downto 0);
	memOut        : out STD_LOGIC_VECTOR(31 downto 0);
	Cout          : out std_logic;
	Overflow      : out std_logic; 
	Zero 	      : out std_logic);
  end component;

  -- Temporary signals to connect to the dff component.
  signal clock, reset, regWrite, Arithmetic, shift_l, ALUorBS, ALUSrc, memWrite, memToReg, Cout, Overflow, Zero : std_logic;
  signal reg0, reg1, ASout, memOut                                               : std_logic_vector(31 downto 0);
  signal immediate                                                               : std_logic_vector(15 downto 0);               
  signal rs, rt, regAddress                                                      : std_logic_vector(4 downto 0);
  signal Control 						                 : std_logic_vector(2 downto 0);										

begin

  DUT: processor port map(clock, reset, rs, rt, regWrite, regAddress, immediate, Control, Arithmetic, shift_l, ALUorBS, ALUSrc, memWrite, memToReg, reg0, reg1, ASout, memOut, Cout, Overflow, Zero);

  -- This process sets the clock value (low for gCLK_HPER, then high
  -- for gCLK_HPER). Absent a "wait" command, processes restart 
  -- at the beginning once they have reached the final statement.
  P_CLK: process
  begin
    clock <= '0';
    wait for gCLK_HPER;
    clock <= '1';
    wait for gCLK_HPER;
  end process;
  
  -- Testbench process  
  P_TB: process
  begin

    	reset    <= '1';
    	regWrite <= '1';
    	wait for cCLK_PER;

    	reset <= '0';
    	wait for cCLK_PER;

        -- Load &A into $25
	rs         <= "00000";
	ALUSrc     <= '1';
	immediate  <= "0000000000000000";
	Control    <= "101";
	ALUorBS    <= '0';
	regAddress <= "11001";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- Load &B into $26
	rs         <= "00000";
	ALUSrc     <= '1';
	immediate  <= "0000000100000000";
	Control    <= "101";
	ALUorBS    <= '0';
	regAddress <= "11010";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- Load A[0] into $1
	rs         <= "11001";
	ALUSrc     <= '1';
	immediate  <= "0000000000000000";
	Control    <= "101";
	ALUorBS    <= '0';
	regAddress <= "00001";
	memToReg   <= '1';
 	wait for cCLK_PER;

	-- Load A[1] into $2
	rs         <= "11001";
	ALUSrc     <= '1';
	immediate  <= "0000000000000100";
	Control    <= "101";
	ALUorBS    <= '0';
	regAddress <= "00010";
	memToReg   <= '1';
 	wait for cCLK_PER;

	-- $3 = $1 + $2
	rs         <= "00001";
	rt         <= "00010";
	ALUSrc     <= '0';
	Control    <= "101";
	ALUorBS    <= '0';
	regAddress <= "00011";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $4 = $1 NOR $1
	rs         <= "00001";
	rt         <= "00001";
	ALUSrc     <= '0';
	Control    <= "100";
	ALUorBS    <= '0';
	regAddress <= "00100";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $5 = $2 NOR $1
	rs         <= "00001";
	rt         <= "00010";
	ALUSrc     <= '0';
	Control    <= "100";
	ALUorBS    <= '0';
	regAddress <= "00101";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $6 = $4 NOR $5
	rs         <= "00100";
	rt         <= "00101";
	ALUSrc     <= '0';
	Control    <= "100";
	ALUorBS    <= '0';
	regAddress <= "00110";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $7 = $4 NAND $5
	rs         <= "00100";
	rt         <= "00101";
	ALUSrc     <= '0';
	Control    <= "011";
	ALUorBS    <= '0';
	regAddress <= "00111";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $8 = $6 NAND $7
	rs         <= "00110";
	rt         <= "00111";
	ALUSrc     <= '0';
	Control    <= "011";
	ALUorBS    <= '0';
	regAddress <= "01000";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $9 = $8 NAND $7
	rs         <= "01000";
	rt         <= "00111";
	ALUSrc     <= '0';
	Control    <= "011";
	ALUorBS    <= '0';
	regAddress <= "01001";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $10 = $8 XOR $9
	rs         <= "01000";
	rt         <= "01001";
	ALUSrc     <= '0';
	Control    <= "010";
	ALUorBS    <= '0';
	regAddress <= "01010";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $11 = $10 XOR $9
	rs         <= "01010";
	rt         <= "01001";
	ALUSrc     <= '0';
	Control    <= "010";
	ALUorBS    <= '0';
	regAddress <= "01011";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $12 = $10 XOR $11
	rs         <= "01010";
	rt         <= "01011";
	ALUSrc     <= '0';
	Control    <= "010";
	ALUorBS    <= '0';
	regAddress <= "01100";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $13 = $12 OR $11
	rs         <= "01100";
	rt         <= "01011";
	ALUSrc     <= '0';
	Control    <= "000";
	ALUorBS    <= '0';
	regAddress <= "01101";
	memToReg   <= '0';
 	wait for cCLK_PER;


	-- $14 = $13 AND $7
	rs         <= "01101";
	rt         <= "00111";
	ALUSrc     <= '0';
	Control    <= "001";
	ALUorBS    <= '0';
	regAddress <= "01110";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $15 = $13 ADD $14
	rs         <= "01101";
	rt         <= "01110";
	ALUSrc     <= '0';
	Control    <= "101";
	ALUorBS    <= '0';
	regAddress <= "01111";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $16 = $3 SLL $2
	rs         <= "00011";
	rt         <= "00010";
	ALUSrc     <= '0';
	ALUorBS    <= '1';
	Arithmetic <= '0';
	shift_l    <= '1';
	regAddress <= "10000";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $17 = $13 SRL $16
	rs         <= "01101";
	rt         <= "10000";
	ALUSrc     <= '0';
	ALUorBS    <= '1';
	Arithmetic <= '0';
	shift_l    <= '0';
	regAddress <= "10001";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $18 = $17 SLL $16
	rs         <= "10001";
	rt         <= "10000";
	ALUSrc     <= '0';
	ALUorBS    <= '1';
	Arithmetic <= '0';
	shift_l    <= '1';
	regAddress <= "10010";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $19 = $18 SLL $3
	rs         <= "10010";
	rt         <= "00011";
	ALUSrc     <= '0';
	ALUorBS    <= '1';
	Arithmetic <= '1';
	shift_l    <= '0';
	regAddress <= "10011";
	memToReg   <= '0';

	-- $20 = $3 SUB $2
	rs         <= "00011";
	rt         <= "00010";
	ALUSrc     <= '0';
	Control    <= "111";
	ALUorBS    <= '0';
	regAddress <= "10100";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $21 = $1 SUB $18
	rs         <= "00001";
	rt         <= "10010";
	ALUSrc     <= '0';
	Control    <= "111";
	ALUorBS    <= '0';
	regAddress <= "10101";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $22 = $1 SUB $17
	rs         <= "00001";
	rt         <= "10001";
	ALUSrc     <= '0';
	Control    <= "111";
	ALUorBS    <= '0';
	regAddress <= "10110";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $23 = $2 SLT $16
	rs         <= "00010";
	rt         <= "10000";
	ALUSrc     <= '0';
	Control    <= "110";
	ALUorBS    <= '0';
	regAddress <= "10111";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $24 = $16 SLT $2
	rs         <= "10000";
	rt         <= "00010";
	ALUSrc     <= '0';
	Control    <= "110";
	ALUorBS    <= '0';
	regAddress <= "11000";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- $27 = $18 SLT $17
	rs         <= "10010";
	rt         <= "10001";
	ALUSrc     <= '0';
	Control    <= "110";
	ALUorBS    <= '0';
	regAddress <= "11011";
	memToReg   <= '0';
 	wait for cCLK_PER;

	-- Store $22 into B[0]
	rs         <= "11010";
	rt         <= "10110";
	ALUSrc     <= '1';
	immediate  <= "0000000000000000";
	Control    <= "101";
	ALUorBS    <= '0';
	memWrite   <= '1';
	regWrite   <= '0';
 	wait for cCLK_PER;

	-- Load B[0] into $1
	rs         <= "11010";
	ALUSrc     <= '1';
	immediate  <= "0000000000000000";
	Control    <= "101";
	ALUorBS    <= '0';
	regAddress <= "00001";
	memToReg   <= '1';
	regWrite   <= '1';
 	wait for cCLK_PER;

	--Display $1
	rt <= "00001";
	wait for cCLK_PER;

  end process;
  
end behavior;