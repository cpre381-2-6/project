library IEEE;
use IEEE.std_logic_1164.all;

entity structural_full_adder is
   port(A    : in std_logic;
	B    : in std_logic;
	Cin  : in std_logic;
	Sum  : out std_logic;
        Cout : out std_logic);

end structural_full_adder;

architecture structure of structural_full_adder is

component andg2
  port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
end component;

component org2
  port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
end component;

component xorg2
  port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
end component;

signal BxorC : std_logic;

signal w : std_logic;
signal x : std_logic;
signal y : std_logic;
signal z : std_logic;


begin

l0: xorg2 port map(B, Cin, BxorC);
l1: xorg2 port map(A, BxorC, Sum);

l2: andg2 port map(B, Cin, w);
l3: andg2 port map(A, B, x);
l4: andg2 port map(A, Cin, y);

l5: org2 port map(w, x, z);
l6: org2 port map(z, y, Cout);

end structure;