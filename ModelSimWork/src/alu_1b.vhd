library IEEE;
use IEEE.std_logic_1164.all;

entity alu_1b is
   port(A         : in std_logic;
	B         : in std_logic;
	B_Inverse : in std_logic;
	Cin       : in std_logic;
	Sel       : in STD_LOGIC_VECTOR (2 downto 0);
	SLT_I     : in std_logic;
	Carryout  : out std_logic;
	Overflow  : out std_logic;
	Result    : out std_logic;
        SLT_O     : out std_logic);
end alu_1b;

architecture structure of alu_1b is

component mux_2
    port ( sel  : in  STD_LOGIC;
           i0   : in  STD_LOGIC;
           i1   : in  STD_LOGIC;
           o    : out STD_LOGIC);
end component;

component mux_8
    port ( sel  : in  STD_LOGIC_VECTOR (2 downto 0);
           i0   : in  STD_LOGIC;
           i1   : in  STD_LOGIC;
           i2   : in  STD_LOGIC;
           i3   : in  STD_LOGIC;
           i4   : in  STD_LOGIC;
           i5   : in  STD_LOGIC;
           i6   : in  STD_LOGIC;
           i7   : in  STD_LOGIC;
           o    : out STD_LOGIC);
end component;

component full_adder
   port(A    : in std_logic;
	B    : in std_logic;
	Cin  : in std_logic;
	Sum  : out std_logic;
        Cout : out std_logic);
end component;

signal not_B, actual_B, or_O, and_O, xor_O, nand_O, nor_O, fa_O, Cout, Overflow_O : std_logic;

begin

notB: not_B <= not B;

bMux: mux_2 port map (B_Inverse, B, not_B, actual_B);

orO: or_O <= A or actual_B;

andO: and_O <= A and actual_B;

xorO: xor_O <= A xor actual_B;

nandO: nand_O <= A nand actual_B;

norO: nor_O <= A nor actual_B;

faO: full_adder port map (A, actual_B, Cin, fa_O, Cout);

resultMux: mux_8 port map (Sel, or_O, and_O, xor_O, nand_O, nor_O, fa_O, SLT_I, '0', Result);

overflow1: Overflow_O <= Cin xor Cout;

overflow2: Overflow <= Overflow_O;

sltO: SLT_O <= Overflow_O xor fa_O;

CarryoutO: Carryout <= Cout;

end structure;