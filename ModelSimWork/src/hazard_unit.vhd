library IEEE;
use IEEE.std_logic_1164.all;

entity hazard_unit is
   port(
	IfId_op        : in std_logic_vector(5 downto 0);
	IfId_func      : in std_logic_vector(5 downto 0);
	IfId_rd        : in std_logic_vector(4 downto 0);
	IfId_rs        : in std_logic_vector(4 downto 0);
	IfId_rt        : in std_logic_vector(4 downto 0);
   
	IdEx_op        : in std_logic_vector(5 downto 0);
	IdEx_func      : in std_logic_vector(5 downto 0);
	IdEx_s_RegWr   : in std_logic;
	IdEx_destination : in std_logic_vector(4 downto 0);
	IdEx_rd        : in std_logic_vector(4 downto 0);
	IdEx_rs        : in std_logic_vector(4 downto 0);
	IdEx_rt        : in std_logic_vector(4 downto 0);
	
	ExMem_op       : in std_logic_vector(5 downto 0);
	ExMem_func     : in std_logic_vector(5 downto 0);
    ExMem_s_RegWr  : in std_logic;
	ExMem_destination : in std_logic_vector(4 downto 0);
	ExMem_rd       : in std_logic_vector(4 downto 0);
	ExMem_rs       : in std_logic_vector(4 downto 0);
	ExMem_rt       : in std_logic_vector(4 downto 0);
	
	MemWb_op       : in std_logic_vector(5 downto 0);
	MemWb_func     : in std_logic_vector(5 downto 0);
	MemWb_s_RegWr  : in std_logic;
	MemWb_rd       : in std_logic_vector(4 downto 0);
	MemWb_rs       : in std_logic_vector(4 downto 0);
	MemWb_rt       : in std_logic_vector(4 downto 0);
	
    Stall_Pc       : out std_logic;
	Flush_Pc       : out std_logic;
	Stall_IfId	   : out std_logic;
	Flush_IfId     : out std_logic;
	Stall_IdEx     : out std_logic;
	Flush_IdEx     : out std_logic;
	Stall_ExMem    : out std_logic;
	Flush_ExMem    : out std_logic;
	Stall_MemWb    : out std_logic;
	Flush_MemWb    : out std_logic);
end hazard_unit;

architecture structure of hazard_unit is

begin

P_IF: process (IfId_op, IfId_func, IfId_rd, IfId_rs, IfId_rt, IdEx_op, IdEx_func, IdEx_rd, IdEx_rs, IdEx_rt, IdEx_s_RegWr, ExMem_op, ExMem_func, ExMem_rd, ExMem_rs, ExMem_rt, ExMem_s_RegWr, ExMem_destination)

 	begin
	
	if ((IdEx_op = "100011") and ((IdEx_rt = IfId_rs) or (IdEx_rt = IfId_rt))) then
		Stall_Pc <= '1';
		Stall_IfId <= '1';
		Flush_IdEx <= '1';
		Flush_IfId <= '1';
	elsif (((IfId_op = "000100") or (IfId_op = "000101")) and ((IfId_rs = IdEx_rd) or (IfId_rs = IdEx_rt) or (IfId_rt = IdEx_rd) or (IfId_rt = IdEx_rt)) and (IdEx_s_RegWr = '1')) then
		Stall_Pc <= '1';
		Stall_IfId <= '1';
		Flush_IdEx <= '1';
		Flush_IfId <= '0';			
	elsif (((IfId_op = "000100") or (IfId_op = "000101")) and ((IfId_rs = ExMem_rd) or (IfId_rs = ExMem_rt) or (IfId_rt = ExMem_rd) or (IfId_rt = ExMem_rt)) and (ExMem_s_RegWr = '1')) then
		Stall_Pc <= '1';
		Stall_IfId <= '1';
		Flush_IdEx <= '1';
		Flush_IfId <= '0';			
	elsif ((IfId_op = "000000") and (IfId_func = "001000")) then 
		if ((ExMem_s_RegWr = '1') and (ExMem_destination = "11111")) then
			Stall_Pc <= '1';
			Stall_IfId <= '1';
			Flush_IdEx <= '1';
			Flush_IfId <= '0';		
		elsif ((IdEx_s_RegWr = '1') and (IdEx_destination = "11111")) then
			Stall_Pc <= '1';
			Stall_IfId <= '1';
			Flush_IdEx <= '1';
			Flush_IfId <= '0';				
		else
			Stall_Pc <= '0';
			Stall_IfId <= '0';
			Flush_IdEx <= '0';
			Flush_IfId <= '1';			
		end if;	
	else 
		Stall_Pc <= '0';
		Stall_IfId <= '0';
		Flush_IdEx <= '0';
		Flush_IfId <= '1';
	end if;
	
	
	Flush_Pc <= '0';
	
	Flush_ExMem <= '0';
	
	Flush_MemWb <= '0';
	
	Stall_IdEx <= '0';
	
	Stall_ExMem <= '0';
	
	Stall_MemWb <= '0';

end process;

end structure;