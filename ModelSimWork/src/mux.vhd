library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux is
    port ( sel  : in  STD_LOGIC_VECTOR (4 downto 0);

           i0   : in  STD_LOGIC_VECTOR (31 downto 0);
           i1   : in  STD_LOGIC_VECTOR (31 downto 0);
           i2   : in  STD_LOGIC_VECTOR (31 downto 0);
           i3   : in  STD_LOGIC_VECTOR (31 downto 0);

           i4   : in  STD_LOGIC_VECTOR (31 downto 0);
           i5   : in  STD_LOGIC_VECTOR (31 downto 0);
           i6   : in  STD_LOGIC_VECTOR (31 downto 0);
           i7   : in  STD_LOGIC_VECTOR (31 downto 0);

           i8   : in  STD_LOGIC_VECTOR (31 downto 0);
           i9   : in  STD_LOGIC_VECTOR (31 downto 0);
           i10  : in  STD_LOGIC_VECTOR (31 downto 0);
           i11  : in  STD_LOGIC_VECTOR (31 downto 0);

           i12  : in  STD_LOGIC_VECTOR (31 downto 0);
           i13  : in  STD_LOGIC_VECTOR (31 downto 0);
           i14  : in  STD_LOGIC_VECTOR (31 downto 0);
           i15  : in  STD_LOGIC_VECTOR (31 downto 0);

           i16  : in  STD_LOGIC_VECTOR (31 downto 0);
           i17  : in  STD_LOGIC_VECTOR (31 downto 0);
           i18  : in  STD_LOGIC_VECTOR (31 downto 0);
           i19  : in  STD_LOGIC_VECTOR (31 downto 0);

           i20  : in  STD_LOGIC_VECTOR (31 downto 0);
           i21  : in  STD_LOGIC_VECTOR (31 downto 0);
           i22  : in  STD_LOGIC_VECTOR (31 downto 0);
           i23  : in  STD_LOGIC_VECTOR (31 downto 0);

           i24  : in  STD_LOGIC_VECTOR (31 downto 0);
           i25  : in  STD_LOGIC_VECTOR (31 downto 0);
           i26  : in  STD_LOGIC_VECTOR (31 downto 0);
           i27  : in  STD_LOGIC_VECTOR (31 downto 0);

           i28  : in  STD_LOGIC_VECTOR (31 downto 0);
           i29  : in  STD_LOGIC_VECTOR (31 downto 0);
           i30  : in  STD_LOGIC_VECTOR (31 downto 0);
           i31  : in  STD_LOGIC_VECTOR (31 downto 0);

           o    : out STD_LOGIC_VECTOR (31 downto 0));
end mux;                   

architecture Behavioral of mux is
begin
process (sel, i0, i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16, i17, i18, i19, i20, i21, i22, i23, i24, i25, i26, i27, i28, i29, i30, i31)
begin

    case sel is
        when "00000" => o <= i0;
        when "00001" => o <= i1;
        when "00010" => o <= i2;
        when "00011" => o <= i3;

        when "00100" => o <= i4;
        when "00101" => o <= i5;
        when "00110" => o <= i6;
        when "00111" => o <= i7;

        when "01000" => o <= i8;
        when "01001" => o <= i9;
        when "01010" => o <= i10;
        when "01011" => o <= i11;

        when "01100" => o <= i12;
        when "01101" => o <= i13;
        when "01110" => o <= i14;
        when "01111" => o <= i15;

        when "10000" => o <= i16;
        when "10001" => o <= i17;
        when "10010" => o <= i18;
        when "10011" => o <= i19;

        when "10100" => o <= i20;
        when "10101" => o <= i21;
        when "10110" => o <= i22;
        when "10111" => o <= i23;

        when "11000" => o <= i24;
        when "11001" => o <= i25;
        when "11010" => o <= i26;
        when "11011" => o <= i27;

        when "11100" => o <= i28;
        when "11101" => o <= i29;
        when "11110" => o <= i30;
	when others  => o <= i31;         
    end case;

end process;
end Behavioral;