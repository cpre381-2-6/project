library IEEE;
use IEEE.std_logic_1164.all;

entity tb_barrel_shifter is

end tb_barrel_shifter;

architecture behavior of tb_barrel_shifter is

component barrel_shifter
   port(input      : in std_logic_vector(31 downto 0);
	shift      : in std_logic_vector(4 downto 0);
	arithmetic : in std_logic;
	shift_l    : in std_logic;
	output     : out std_logic_vector(31 downto 0));
end component;

signal arithmetic, shift_l : std_logic;
signal shift               : std_logic_vector(4 downto 0);
signal input, output       : std_logic_vector(31 downto 0);

begin

DUT0: barrel_shifter port map(input, shift, arithmetic, shift_l, output);

  process
  begin

	-- SHIFT 0
	input <= "10000000000000000000000000001111";
	shift <= "00000";
	arithmetic <= '0';
	shift_l <= '0';
    	wait for 100 ns; -- 8000000F

	-- SHIFT 0
	input <= "10000000000000000000000000001111";
	shift <= "00000";
	arithmetic <= '1';
	shift_l <= '0';
    	wait for 100 ns; -- 8000000F

	-- SHIFT 1
	input <= "10000000000000000000000000001111";
	shift <= "00001";
	arithmetic <= '0';
	shift_l <= '0';
    	wait for 100 ns; -- 40000007

	-- SHIFT 1
	input <= "10000000000000000000000000001111";
	shift <= "00001";
	arithmetic <= '1';
	shift_l <= '0';
    	wait for 100 ns; -- C0000007

	-- SHIFT 1
	input <= "10000000000000000000000000001111";
	shift <= "00001";
	arithmetic <= '0';
	shift_l <= '1';
    	wait for 100 ns; -- 0000001E

	-- SHIFT 2
	input <= "10000000000000000000000000001111";
	shift <= "00010";
	arithmetic <= '0';
	shift_l <= '0';
    	wait for 100 ns; -- 20000003

	-- SHIFT 3
	input <= "10000000000000000000000000001111";
	shift <= "00011";
	arithmetic <= '0';
	shift_l <= '0';
    	wait for 100 ns; -- 10000001

	-- SHIFT 4
	input <= "10000000000000000000000000001111";
	shift <= "00100";
	arithmetic <= '0';
	shift_l <= '0';
    	wait for 100 ns; -- 08000000

	-- SHIFT 8
	input <= "10000000000000000000000000001111";
	shift <= "01000";
	arithmetic <= '0';
	shift_l <= '0';
    	wait for 100 ns; -- 00800000

	-- SHIFT 8
	input <= "10000000000000000000000000001111";
	shift <= "01000";
	arithmetic <= '1';
	shift_l <= '0';
    	wait for 100 ns; -- FF800000

	-- SHIFT 16
	input <= "10000000000000000000000000001111";
	shift <= "10000";
	arithmetic <= '0';
	shift_l <= '0';
    	wait for 100 ns; -- 00008000

  end process;

end behavior;