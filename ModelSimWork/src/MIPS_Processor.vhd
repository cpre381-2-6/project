-------------------------------------------------------------------------
-- Henry Duwe
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- MIPS_Processor.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a skeleton of a MIPS_Processor
-- implementation.

-- 01/29/2019 by H3::Design created.
-------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;

entity MIPS_Processor is
  generic(N : integer := 32);
  port(iCLK            : in std_logic;
       iRST            : in std_logic;
       iInstLd         : in std_logic;
       iInstAddr       : in std_logic_vector(N-1 downto 0);
       iInstExt        : in std_logic_vector(N-1 downto 0);
       oALUOut         : out std_logic_vector(N-1 downto 0)); -- TODO: Hook this up to the output of the ALU. It is important for synthesis that you have this output that can effectively be impacted by all other components so they are not optimized away.

end  MIPS_Processor;


architecture structure of MIPS_Processor is

  -- Required data memory signals
  signal s_DMemWr       : std_logic; -- TODO: use this signal as the final active high data memory write enable signal
  signal s_DMemAddr     : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory address input
  signal s_DMemData     : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory data input
  signal s_DMemOut      : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the data memory output

  -- Required register file signals
  signal s_RegWr        : std_logic; -- TODO: use this signal as the final active high write enable input to the register file
  signal s_RegWrAddr    : std_logic_vector(4 downto 0); -- TODO: use this signal as the final destination register address input
  signal s_RegWrData    : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory data input
  signal s_d0           : std_logic_vector(N-1 downto 0);
  signal s_d1           : std_logic_vector(N-1 downto 0);
  signal s_RtRd         : std_logic_vector(4 downto 0);
  signal s_AluMem       : std_logic_vector(N-1 downto 0);

  -- Required instruction memory signals
  signal s_IMemAddr     : std_logic_vector(N-1 downto 0); -- Do not assign this signal, assign to s_NextInstAddr instead
  signal s_NextInstAddr : std_logic_vector(N-1 downto 0); -- TODO: use this signal as your intended final instruction memory address input.
  signal s_Inst         : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the instruction signal

  -- Required halt signal -- for simulation
  signal v0             : std_logic_vector(N-1 downto 0); -- TODO: should be assigned to the output of register 2, used to implement the halt SYSCALL
  signal s_Halt         : std_logic;  -- TODO: this signal indicates to the simulation that intended program execution has completed. This case happens when the syscall instruction is observed and the V0 register is at 0x0000000A. This signal is active high and should only be asserted after the last register and memory writes before the syscall are guaranteed to be completed.

  -- Required PC / Instruction Adder / Branch / Jump signals
  signal s_PcPlus4         : std_logic_vector(N-1 downto 0);
  signal s_NextPcIn        : std_logic_vector(N-1 downto 0);
  signal s_InstAddCout     : std_logic;
  signal s_JA              : std_logic_vector(N-1 downto 0);
  signal s_PcBranchJump    : std_logic_vector(N-1 downto 0);
  signal s_BranchSel       : std_logic;
  signal s_PcBranchJumpSel : std_logic;
  signal s_BranchAdderCout : std_logic;
  signal s_BranchAddr      : std_logic_vector(N-1 downto 0);
  signal s_BranchJumpAddr  : std_logic_vector(N-1 downto 0);
  signal s_JumpAddr        : std_logic_vector(N-1 downto 0);
  signal s_BranchMuxOut    : std_logic;
  signal s_BranchSignal    : std_logic;

  -- Required Control signals
  signal ALUControl     : std_logic_vector(2 downto 0);
  signal ALUorBS        : std_logic;
  signal ALUSrc         : std_logic;
  signal Arithmetic     : std_logic;
  signal BNE        	: std_logic;
  signal Branch         : std_logic;
  signal J          	: std_logic;
  signal JAL        	: std_logic;
  signal JR         	: std_logic;
  signal MemToReg       : std_logic;
  signal RegDst         : std_logic;
  signal Shift_L        : std_logic;
  signal Shift_Sel      : std_logic_vector(1 downto 0);
  signal Unsign         : std_logic;
  signal Zero_Ext       : std_logic;

  -- Required Sign Extender signal
  signal s_SignExt      : std_logic_vector(N-1 downto 0);
  signal s_ZeroExt      : std_logic_vector(N-1 downto 0);
  signal s_Extended     : std_logic_vector(N-1 downto 0);

  -- Required ALU signals
  signal s_BInput       : std_logic_vector(N-1 downto 0);
  signal s_Shamt        : std_logic_vector(4 downto 0);
  signal s_Carryout     : std_logic;
  signal s_Overflow     : std_logic;
  signal s_Zero         : std_logic;

  -- Required Shift Signal
  signal s_ShamtMux     : std_logic_vector(4 downto 0);
  
  -- IFID
  
  signal IfId_FLUSH, IfId_STALL : std_logic;
  signal IfIdInstruction, IfIdPcAddress : std_logic_vector(31 downto 0);
  
  component IFID is
	port(clock         : in std_logic;
		 flush         : in std_logic;
		 stall         : in std_logic;
		 i_instruction : in std_logic_vector(31 downto 0);
		 i_PcAddress   : in std_logic_vector(31 downto 0);
		 o_instruction : out std_logic_vector(31 downto 0);
		 o_PcAddress   : out std_logic_vector(31 downto 0));  
  end component;
  
  -- IDEX
  
  signal IdEx_FLUSH, IdEx_STALL : std_logic;
  signal IdEx_ALUorBS, IdEx_ALUSrc, IdEx_Arithmetic, IdEx_JAL, IdEx_MemToReg, IdEx_RegDst, IdEx_s_DMemWr, IdEx_s_RegWr, IdEx_Shift_L, IdEx_Unsign : std_logic; 
  signal IdEx_ALUControl : std_logic_vector(2 downto 0);
  signal IdEx_Shift_Sel : std_logic_vector(1 downto 0);
  signal IdEx_PcAddress, IdEx_d0, IdEx_d1, IdEx_extended : std_logic_vector(31 downto 0);
  signal IdEx_rs, IdEx_rt, IdEx_rd, IdEx_shamt : std_logic_vector(4 downto 0);
  signal IdEx_op, IdEx_func : std_logic_vector(5 downto 0);
  signal s_DMemWr_Carry, s_RegWr_Carry : std_logic;
  
  component IDEX
    port(clock        : in std_logic;
  		 flush        : in std_logic;
  		 stall        : in std_logic;

  		 i_ALUControl : in STD_LOGIC_VECTOR(2 downto 0);
  		 i_ALUorBS    : in STD_LOGIC;
  		 i_ALUSrc     : in STD_LOGIC;
  		 i_Arithmetic : in STD_LOGIC;

  		 i_JAL        : in STD_LOGIC;

  		 i_MemToReg   : in STD_LOGIC;
  		 i_RegDst     : in STD_LOGIC;
  		 i_s_DMemWr   : in STD_LOGIC;
  		 i_s_RegWr    : in STD_LOGIC;
  		 i_Shift_L    : in STD_LOGIC;
  		 i_Shift_Sel  : in STD_LOGIC_VECTOR(1 downto 0);
  		 i_Unsign     : in STD_LOGIC;
		 
		 i_op         : in STD_LOGIC_VECTOR(5 downto 0);
		 i_func       : in STD_LOGIC_VECTOR(5 downto 0);
		 
		 i_PcAddress  : in std_logic_vector(31 downto 0);

  		 i_d0		  : in std_logic_vector(31 downto 0);
  		 i_d1		  : in std_logic_vector(31 downto 0);
  		 i_extended   : in std_logic_vector(31 downto 0);
  		 i_rs         : in std_logic_vector(4 downto 0);
  		 i_rt         : in std_logic_vector(4 downto 0);
  		 i_rd         : in std_logic_vector(4 downto 0);
  		 i_shamt      : in std_logic_vector(4 downto 0);

  		 o_ALUControl : out STD_LOGIC_VECTOR(2 downto 0);
  		 o_ALUorBS    : out STD_LOGIC;
  		 o_ALUSrc     : out STD_LOGIC;
  		 o_Arithmetic : out STD_LOGIC;

  		 o_JAL        : out STD_LOGIC;

  		 o_MemToReg   : out STD_LOGIC;
  		 o_RegDst     : out STD_LOGIC;
  		 o_s_DMemWr   : out STD_LOGIC;
  		 o_s_RegWr    : out STD_LOGIC;
  		 o_Shift_L    : out STD_LOGIC;
  		 o_Shift_Sel  : out STD_LOGIC_VECTOR(1 downto 0);
  		 o_Unsign     : out STD_LOGIC;
		 
	     o_op         : out STD_LOGIC_VECTOR(5 downto 0);
		 o_func       : out STD_LOGIC_VECTOR(5 downto 0);
		 
		 o_PcAddress  : out std_logic_vector(31 downto 0);

  		 o_d0         : out std_logic_vector(31 downto 0);
  		 o_d1		  : out std_logic_vector(31 downto 0);
  		 o_extended   : out std_logic_vector(31 downto 0);
  		 o_rs         : out std_logic_vector(4 downto 0);
  		 o_rt         : out std_logic_vector(4 downto 0);
  		 o_rd         : out std_logic_vector(4 downto 0);
  		 o_shamt      : out std_logic_vector(4 downto 0));
  end component;
  
  -- EXMEM
  
  signal ExMem_FLUSH, ExMem_STALL : std_logic;
  signal ExMem_JAL, ExMem_MemToReg, ExMem_s_DMemWr, ExMem_s_RegWr : std_logic;
  signal ExMem_PcAddress, ExMem_ALUResult, ExMem_d1, ALUResult : std_logic_vector(31 downto 0);
  signal ExMem_destination, ExMem_rs, ExMem_rt, ExMem_rd, s_RtRd_Carry0, s_RtRd_Carry1 : std_logic_vector(4 downto 0);
  signal ExMem_op, ExMem_func : std_logic_vector(5 downto 0);
  
  component EXMEM
    port(	
		clock         : in std_logic;
  		flush         : in std_logic;
  		stall         : in std_logic;

  		i_JAL         : in STD_LOGIC;
		
  		i_MemToReg    : in STD_LOGIC;
  		i_s_DMemWr    : in STD_LOGIC;
  		i_s_RegWr     : in STD_LOGIC;

  		i_ALUResult   : in std_logic_vector(31 downto 0);
  		i_d1          : in std_logic_vector(31 downto 0);
  		i_destination : in std_logic_vector(4 downto 0);
		
		i_op          : in STD_LOGIC_VECTOR(5 downto 0);
		i_func        : in STD_LOGIC_VECTOR(5 downto 0);
		
		i_PcAddress   : in std_logic_vector(31 downto 0);

  		i_rs          : in std_logic_vector(4 downto 0);
  		i_rt          : in std_logic_vector(4 downto 0);
  		i_rd          : in std_logic_vector(4 downto 0);

  		o_JAL         : out STD_LOGIC;

  		o_MemToReg    : out STD_LOGIC;
  		o_s_DMemWr    : out STD_LOGIC;
  		o_s_RegWr     : out STD_LOGIC;

  		o_ALUResult   : out std_logic_vector(31 downto 0);
  		o_d1          : out std_logic_vector(31 downto 0);
  		o_destination : out std_logic_vector(4 downto 0);
		
	    o_op          : out STD_LOGIC_VECTOR(5 downto 0);
		o_func        : out STD_LOGIC_VECTOR(5 downto 0);
		
		o_PcAddress   : out std_logic_vector(31 downto 0);

  		o_rs          : out std_logic_vector(4 downto 0);
  		o_rt          : out std_logic_vector(4 downto 0);
  		o_rd          : out std_logic_vector(4 downto 0));
  end component;
  
  -- MEMWB
  
  signal MemWb_JAL, MemWb_FLUSH, MemWb_STALL, MemWb_MemToReg, MemWb_s_RegWr : std_logic;
  signal MemWb_destination, MemWb_rs, MemWb_rt, MemWb_rd : std_logic_vector(4 downto 0);
  signal MemWb_PcAddress, MemWb_ALUResult, MemWb_d1, MemWB_MemOut : std_logic_vector(31 downto 0);
  signal MemWb_op, MemWb_func : std_logic_vector(5 downto 0);
  
  component MEMWB
    port(	clock         : in std_logic;
			flush         : in std_logic;
			stall         : in std_logic;
			
			i_op          : in std_logic_vector(5 downto 0);
			i_func        : in std_logic_vector(5 downto 0);
			
			i_PcAddress   : in std_logic_vector(31 downto 0);
			
			i_JAL         : in std_logic;

			i_MemToReg    : in STD_LOGIC;
			i_s_RegWr     : in STD_LOGIC;
			i_d1          : in std_logic_vector(31 downto 0);
			i_ALUResult   : in std_logic_vector(31 downto 0);
			i_MemOut      : in std_logic_vector(31 downto 0);
			i_destination : in std_logic_vector(4 downto 0);
			i_rs          : in std_logic_vector(4 downto 0);
			i_rt          : in std_logic_vector(4 downto 0);
			i_rd          : in std_logic_vector(4 downto 0);
			
			o_op          : out std_logic_vector(5 downto 0);
			o_func        : out std_logic_vector(5 downto 0);
			
			o_PcAddress   : out std_logic_vector(31 downto 0);
			
			o_JAL         : out std_logic;

			o_MemToReg    : out STD_LOGIC;
			o_s_RegWr     : out STD_LOGIC;
			o_d1          : out std_logic_vector(31 downto 0);
			o_ALUResult   : out std_logic_vector(31 downto 0);
			o_MemOut      : out std_logic_vector(31 downto 0);
			o_destination : out std_logic_vector(4 downto 0);
			o_rs          : out std_logic_vector(4 downto 0);
			o_rt          : out std_logic_vector(4 downto 0);
			o_rd          : out std_logic_vector(4 downto 0));
  end component;
  
  -- Forward / Stall
  
  signal ForwardA, ForwardB, ForwardC : std_logic_vector(1 downto 0);
  signal s_ForwardA0, s_ForwardA1, s_ForwardB0, s_ForwardB1, s_ForwardC0, s_ForwardC1 : std_logic_vector(31 downto 0);
  signal Pc_STALL, Pc_FLUSH : std_logic;

  component forward_unit is
    port(
		IdEx_op        : in std_logic_vector(5 downto 0);
		IdEx_func      : in std_logic_vector(5 downto 0);
		IdEx_s_RegWr   : in std_logic;
		IdEx_rd        : in std_logic_vector(4 downto 0);
		IdEx_rs        : in std_logic_vector(4 downto 0);
		IdEx_rt        : in std_logic_vector(4 downto 0);
		
		ExMem_op       : in std_logic_vector(5 downto 0);
		ExMem_func     : in std_logic_vector(5 downto 0);
		ExMem_s_RegWr  : in std_logic;
		ExMem_rd       : in std_logic_vector(4 downto 0);
		ExMem_rs       : in std_logic_vector(4 downto 0);
		ExMem_rt       : in std_logic_vector(4 downto 0);
		
		MemWb_op       : in std_logic_vector(5 downto 0);
		MemWb_func     : in std_logic_vector(5 downto 0);
		MemWb_s_RegWr  : in std_logic;
		MemWb_rd       : in std_logic_vector(4 downto 0);
		MemWb_rs       : in std_logic_vector(4 downto 0);
		MemWb_rt       : in std_logic_vector(4 downto 0);
		
		ForwardA       : out std_logic_vector(1 downto 0);
		ForwardB       : out std_logic_vector(1 downto 0);
		ForwardC       : out std_logic_vector(1 downto 0));
  end component;
  
  component hazard_unit is
   port(
	IfId_op        : in std_logic_vector(5 downto 0);
	IfId_func      : in std_logic_vector(5 downto 0);
	IfId_rd		   : in std_logic_vector(4 downto 0);
	IfId_rs		   : in std_logic_vector(4 downto 0);
	IfId_rt		   : in std_logic_vector(4 downto 0);
   
	IdEx_op        : in std_logic_vector(5 downto 0);
	IdEx_func      : in std_logic_vector(5 downto 0);
	IdEx_s_RegWr   : in std_logic;
	IdEx_destination : in std_logic_vector(4 downto 0);
	IdEx_rd        : in std_logic_vector(4 downto 0);
	IdEx_rs        : in std_logic_vector(4 downto 0);
	IdEx_rt        : in std_logic_vector(4 downto 0);
	
	ExMem_op       : in std_logic_vector(5 downto 0);
	ExMem_func     : in std_logic_vector(5 downto 0);
    ExMem_s_RegWr  : in std_logic;
	ExMem_destination : in std_logic_vector(4 downto 0);
	ExMem_rd       : in std_logic_vector(4 downto 0);
	ExMem_rs       : in std_logic_vector(4 downto 0);
	ExMem_rt       : in std_logic_vector(4 downto 0);
	
	MemWb_op       : in std_logic_vector(5 downto 0);
	MemWb_func     : in std_logic_vector(5 downto 0);
	MemWb_s_RegWr  : in std_logic;
	MemWb_rd       : in std_logic_vector(4 downto 0);
	MemWb_rs       : in std_logic_vector(4 downto 0);
	MemWb_rt       : in std_logic_vector(4 downto 0);
	
    Stall_Pc       : out std_logic;
	Flush_Pc       : out std_logic;
	Stall_IfId	   : out std_logic;
	Flush_IfId     : out std_logic;
	Stall_IdEx     : out std_logic;
	Flush_IdEx     : out std_logic;
	Stall_ExMem    : out std_logic;
	Flush_ExMem    : out std_logic;
	Stall_MemWb    : out std_logic;
	Flush_MemWb    : out std_logic);  
  end component;

  component mem is
    generic(ADDR_WIDTH : integer;
            DATA_WIDTH : integer);
    port(clk          : in std_logic;
         addr         : in std_logic_vector((ADDR_WIDTH-1) downto 0);
         data         : in std_logic_vector((DATA_WIDTH-1) downto 0);
         we           : in std_logic := '1';
         q            : out std_logic_vector((DATA_WIDTH -1) downto 0));
    end component;

  component ALU is
    port(A            : in std_logic_vector(31 downto 0);
         ALUorBS      : in std_logic;
         Arithmetic   : in std_logic;
       	 B            : in std_logic_vector(31 downto 0);
       	 Control      : in std_logic_vector(2 downto 0);
         Shift_Amount : in std_logic_vector(4 downto 0);
       	 Shift_Left   : in std_logic;
         Unsign       : in std_logic;
       	 Result       : out std_logic_vector(31 downto 0);
       	 Carryout     : out std_logic;
       	 Overflow     : out std_logic;
       	 Zero         : out std_logic);
  end component;
  
  component branch_comp is
   port(
    A        : in std_logic_vector(31 downto 0);
	B        : in std_logic_vector(31 downto 0);
    S        : out std_logic);  
  end component;

  component control is
    port(func : in STD_LOGIC_VECTOR(5 downto 0);
  	     op   : in STD_LOGIC_VECTOR(5 downto 0);
  	     ALUControl : out STD_LOGIC_VECTOR(2 downto 0);
  	     ALUorBS    : out STD_LOGIC;
  	     ALUSrc     : out STD_LOGIC;
  	     Arithmetic : out STD_LOGIC;
		 BNE        : out STD_LOGIC;
		 Branch     : out STD_LOGIC;
		 J          : out STD_LOGIC;
		 JAL        : out STD_LOGIC;
		 JR         : out STD_LOGIC;
  	     MemToReg   : out STD_LOGIC;
  	     RegDst     : out STD_LOGIC;
  	     s_DMemWr   : out STD_LOGIC;
  	     s_RegWr    : out STD_LOGIC;
  	     Shift_L    : out STD_LOGIC;
  	     Shift_Sel  : out STD_LOGIC_VECTOR(1 downto 0);
  	     Unsign     : out STD_LOGIC;
		 Zero_Ext   : out STD_LOGIC);
  end component;
  
  component mux_2 is
    port (sel  : in  STD_LOGIC;
          i0   : in  STD_LOGIC;
          i1   : in  STD_LOGIC;
          o    : out STD_LOGIC);  
  end component;

  component n_bit_register is
    generic(N : integer := 32);
    port(i_CLK        : in std_logic;
         i_RST        : in std_logic;
         i_WE         : in std_logic;
         i_D          : in std_logic_vector(N-1 downto 0);
         o_Q          : out std_logic_vector(N-1 downto 0));
    end component;

  component register_file is
    port( clock         : in STD_LOGIC;
	reset         : in STD_LOGIC;
	d0            : out STD_LOGIC_VECTOR(31 downto 0);
	d1            : out STD_LOGIC_VECTOR(31 downto 0);
	reg2          : out STD_LOGIC_VECTOR(31 downto 0);
	r0            : in STD_LOGIC_VECTOR(4 downto 0);
	r1            : in STD_LOGIC_VECTOR(4 downto 0);
	register_data : in STD_LOGIC_VECTOR(31 downto 0);
	register_num  : in STD_LOGIC_VECTOR(4 downto 0);
        write_enable  : in STD_LOGIC);
  end component;

  component sign_extender
    port(input  : in std_logic_vector(15 downto 0);
         output : out std_logic_vector(31 downto 0));
  end component;

  component structural_mux_n_bit is
    generic(N : integer := 32);
    port(i0  : in std_logic_vector(N-1 downto 0);
         i1  : in std_logic_vector(N-1 downto 0);
         s   : in std_logic;
         o   : out std_logic_vector(N-1 downto 0));
  end component;

  component structural_full_adder_n is
    generic(N : integer := 32);
    port(A    : in std_logic_vector(N-1 downto 0);
 	       B    : in std_logic_vector(N-1 downto 0);
 	       Cin  : in std_logic;
 	       Sum  : out std_logic_vector(N-1 downto 0);
         Cout : out std_logic);
    end component;
	
  component zero_extender
     port(input  : in std_logic_vector(15 downto 0);
          output : out std_logic_vector(31 downto 0));
  end component;

begin

  -- TODO: This is required to be your final input to your instruction memory. This provides a feasible method to externally load the memory module which means that the synthesis tool must assume it knows nothing about the values stored in the instruction memory. If this is not included, much, if not all of the design is optimized out because the synthesis tool will believe the memory to be all zeros.
  with iInstLd select
    s_IMemAddr <= s_NextInstAddr when '0',
      iInstAddr when others;


  IMem: mem
    generic map(ADDR_WIDTH => 10,
                DATA_WIDTH => N)
    port map(clk  => iCLK,
             addr => s_IMemAddr(11 downto 2),
             data => iInstExt,
             we   => iInstLd,
             q    => s_Inst);

  DMem: mem
    generic map(ADDR_WIDTH => 10,
                DATA_WIDTH => N)
    port map(clk  => iCLK,
             addr => s_DMemAddr(11 downto 2),
             data => s_DMemData,
             we   => s_DMemWr,
             q    => s_DMemOut);
  
  s_JA <= "0000" & IfIdInstruction(25 downto 0) & "00";
  
  BranchAdder: structural_full_adder_n
    generic map(N => 32)
    port map (A    => IfIdPcAddress,
  	          B    => s_Extended(29 downto 0) & "00",
  	          Cin  => '0',
   	          Sum  => s_BranchAddr,
              Cout => s_BranchAdderCout);
			  
  BranchMux: mux_2
    port map (sel => BNE,
			  i0  => s_BranchSignal,
              i1  => not s_BranchSignal,
              o   => s_BranchMuxOut);  
  
  s_BranchSel <= s_BranchMuxOut AND Branch;
  
  JumpMux: structural_mux_n_bit
    generic map (N => 32)
    port map (i0 => s_JA,
              i1 => s_d0,
              s  => JR,
              o  => s_JumpAddr);  
  
  BranchJumpMux: structural_mux_n_bit
    generic map (N => 32)
    port map (i0 => s_JumpAddr,
              i1 => s_BranchAddr,
              s  => s_BranchSel,
              o  => s_BranchJumpAddr);
  
  s_PcBranchJumpSel <= s_BranchSel OR J;
  
  PcBranchJumpMux: structural_mux_n_bit
    generic map (N => 32)
    port map (i0 => s_PcPlus4,
              i1 => s_BranchJumpAddr,
              s  => s_PcBranchJumpSel,
              o  => s_PcBranchJump);
  
  PcResetMux: structural_mux_n_bit
    generic map (N => 32)
    port map (i0 => s_PcBranchJump,
              i1 => x"00400000",
              s  => Pc_FLUSH or iRST,
              o  => s_NextPcIn);

  PC: n_bit_register
    generic map (N => 32)
    port map (i_CLK => iCLK,
              i_RST => '0',
              i_WE  => not Pc_STALL,
              i_D   => s_NextPcIn,
              o_Q   => s_NextInstAddr);

  InstructionAdder: structural_full_adder_n
    generic map(N => 32)
    port map (A    => s_NextInstAddr,
 	          B    => x"00000004",
 	          Cin  => '0',
 	          Sum  => s_PcPlus4,
              Cout => s_InstAddCout);
			  
  IFIDRegister: IFID 
    port map (clock         => iCLK,
	          flush         => ((IfId_FLUSH and s_PcBranchJumpSel) or iRST),
	          stall         => IfId_STALL,
	          i_instruction => s_Inst,
	          i_PcAddress   => s_PcPlus4,
	          o_instruction => IfIdInstruction,
	          o_PcAddress   => IfIdPcAddress);
			  
  HazardUnit: hazard_unit
	   port map(
	    IfId_op        => IfIdInstruction(31 downto 26),
		IfId_func      => IfIdInstruction(5 downto 0),
		IfId_rd        => IfIdInstruction(15 downto 11),
		IfId_rs        => IfIdInstruction(25 downto 21),
		IfId_rt        => IfIdInstruction(20 downto 16),
	   
	    IdEx_op        => IdEx_op,
		IdEx_func      => IdEx_func,
		IdEx_s_RegWr   => IdEx_s_RegWr,
		IdEx_Destination => s_RtRd_Carry1,
		IdEx_rd        => IdEx_rd,
		IdEx_rs        => IdEx_rs,
		IdEx_rt        => IdEx_rt,
		
	    ExMem_op       => ExMem_op,
		ExMem_func     => ExMem_func,
		ExMem_s_RegWr  => ExMem_s_RegWr ,
		ExMem_destination => ExMem_destination,
		ExMem_rd       => ExMem_rd,
		ExMem_rs       => ExMem_rs,
		ExMem_rt       => ExMem_rt,
		
	    MemWb_op       => MemWb_op,
		MemWb_func     => MemWb_func,
		MemWb_s_RegWr  => MemWb_s_RegWr,
		MemWb_rd       => MemWb_rd,
		MemWb_rs       => MemWb_rs,
		MemWb_rt       => MemWb_rt,
		
		Stall_Pc       => Pc_STALL,
		Flush_Pc       => Pc_FLUSH,
		Stall_IfId	   => IfId_STALL,
		Flush_IfId     => IfId_FLUSH,
		Stall_IdEx     => IdEx_STALL,
		Flush_IdEx     => IdEx_FLUSH,
		Stall_ExMem    => ExMem_STALL,
		Flush_ExMem    => ExMem_FLUSH,
		Stall_MemWb    => MemWb_STALL,
		Flush_MemWb    => MemWb_FLUSH);

  ControlModule: control
    port map (func       => IfIdInstruction(5 downto 0),
  	          op         => IfIdInstruction(31 downto 26),
  	          ALUControl => ALUControl,
  	          ALUorBS    => ALUorBS,
  	          ALUSrc     => ALUSrc,
  	          Arithmetic => Arithmetic,
			  BNE 		 => BNE,
			  Branch     => Branch,
			  J 		 => J,
			  JAL 		 => JAL,
			  JR 		 => JR,
  	          MemToReg   => MemToReg,
  	          RegDst     => RegDst,
  	          s_DMemWr   => s_DMemWr_Carry,
  	          s_RegWr    => s_RegWr_Carry,
  	          Shift_L    => Shift_L,
  	          Shift_Sel  => Shift_Sel,
  	          Unsign     => Unsign,
			  Zero_Ext   => Zero_Ext);
			  
  WdMux: structural_mux_n_bit
	generic map (N => 32)
    port map (i0 => s_AluMem,
              i1 => MemWb_PcAddress,
              s  => MemWb_JAL,
              o  => s_RegWrData);

  RegisterFile: register_file
    port map(clock         => iCLK,
  	         reset         => iRST,
  	         d0            => s_d0,
  	         d1            => s_d1,
			 reg2          => v0,
  	         r0            => IfIdInstruction(25 downto 21),
  	         r1            => IfIdInstruction(20 downto 16),
  	         register_data => s_RegWrData,
  	         register_num  => s_RegWrAddr,
             write_enable  => s_RegWr);
			 
  BranchComp: branch_comp
	port map(A => s_d0,
			 B => s_d1,
			 S => s_BranchSignal);

  SignExtender: sign_extender
    port map(input  => IfIdInstruction(15 downto 0),
             output => s_SignExt);
			 
  ZeroExtender: zero_extender
	port map(input  => IfIdInstruction(15 downto 0),
             output => s_ZeroExt);
			 
  ExtendMux: structural_mux_n_bit
    generic map (N => 32)
    port map(i0 => s_SignExt,
             i1 => s_ZeroExt,
             s  => Zero_Ext,
             o  => s_Extended);
		 
  IDEXRegister: IDEX 
	port map(
		clock        => iCLK,
  		flush        => IdEx_FLUSH or iRST,
  		stall        => IdEx_STALL,

  		i_ALUControl => ALUControl,
  		i_ALUorBS    => ALUorBS,
  		i_ALUSrc     => ALUSrc,
  		i_Arithmetic => Arithmetic,

  		i_JAL        => JAL,

  		i_MemToReg   => MemToReg,
  		i_RegDst     => RegDst,
  		i_s_DMemWr   => s_DMemWr_Carry,
  		i_s_RegWr    => s_RegWr_Carry,
  		i_Shift_L    => Shift_L ,
  		i_Shift_Sel  => Shift_Sel, 
  		i_Unsign     => Unsign,
		
		i_op         => IfIdInstruction(31 downto 26),
		i_func       => IfIdInstruction(5 downto 0),
		
		i_PcAddress  => IfIdPcAddress,

  		i_d0		 => s_d0,
  		i_d1		 => s_d1,
  		i_extended   => s_Extended,
  		i_rs         => IfIdInstruction(25 downto 21),
  		i_rt         => IfIdInstruction(20 downto 16),
  		i_rd         => IfIdInstruction(15 downto 11),
  		i_shamt      => IfIdInstruction(10 downto 6),

  		o_ALUControl => IdEx_ALUControl,
  		o_ALUorBS    => IdEx_ALUorBS,
  		o_ALUSrc     => IdEx_ALUSrc,
  		o_Arithmetic => IdEx_Arithmetic,

  		o_JAL        => IdEx_JAL,
		
  		o_MemToReg   => IdEx_MemToReg,
  		o_RegDst     => IdEx_RegDst,
  		o_s_DMemWr   => IdEx_s_DMemWr,
  		o_s_RegWr    => IdEx_s_RegWr,
  		o_Shift_L    => IdEx_Shift_L ,
  		o_Shift_Sel  => IdEx_Shift_Sel, 
  		o_Unsign     => IdEx_Unsign,
		
		o_op         => IdEx_op,
		o_func       => IdEx_func,
		
		o_PcAddress  => IdEx_PcAddress,

  		o_d0         => IdEx_d0,
  		o_d1		 => IdEx_d1,
  		o_extended   => IdEx_extended,
  		o_rs         => IdEx_rs,
  		o_rt         => IdEx_rt,
  		o_rd         => IdEx_rd,
  		o_shamt      => IdEx_shamt);

  AluBMux: structural_mux_n_bit
    generic map (N => 32)
    port map(i0 => IdEx_d1,
             i1 => IdEx_Extended,
             s  => IdEx_ALUSrc,
             o  => s_BInput);
			 
  ForwardA0Mux: structural_mux_n_bit
    generic map (N => 32)
    port map(i0 => IdEx_d0,
             i1 => ExMem_ALUResult,
             s  => ForwardA(0),
             o  => s_ForwardA0);
			 
  ForwardA1Mux: structural_mux_n_bit
    generic map (N => 32)
    port map(i0 => s_ForwardA0,
             i1 => s_AluMem,
             s  => ForwardA(1),
             o  => s_ForwardA1);
			 
  ForwardB0Mux: structural_mux_n_bit
    generic map (N => 32)
    port map(i0 => s_BInput,
             i1 => ExMem_ALUResult,
             s  => ForwardB(0),
             o  => s_ForwardB0);
			 
  ForwardB1Mux: structural_mux_n_bit
    generic map (N => 32)
    port map(i0 => s_ForwardB0,
             i1 => s_AluMem,
             s  => ForwardB(1),
             o  => s_ForwardB1);
			 
  ForwardC0Mux: structural_mux_n_bit
    generic map (N => 32)
    port map(i0 => IdEx_d1,
             i1 => ExMem_ALUResult,
             s  => ForwardC(0),
             o  => s_ForwardC0);
			 
  ForwardC1Mux: structural_mux_n_bit
    generic map (N => 32)
    port map(i0 => s_ForwardC0,
             i1 => s_AluMem,
             s  => ForwardC(1),
             o  => s_ForwardC1);

  AluComponent: ALU
    port map(A            => s_ForwardA1,
             ALUorBS      => IdEx_ALUorBS,
             Arithmetic   => IdEx_Arithmetic,
       	     B            => s_ForwardB1,
       	     Control      => IdEx_ALUControl,
             Shift_Amount => s_Shamt,
       	     Shift_Left   => IdEx_Shift_L,
             Unsign       => IdEx_Unsign,
       	     Result       => ALUResult,
       	     Carryout     => s_Carryout,
       	     Overflow     => s_Overflow,
       	     Zero         => s_Zero);

   ShamtMux0: structural_mux_n_bit
     generic map (N => 5)
     port map(i0 => IdEx_shamt,
              i1 => s_ForwardA1(4 downto 0),
              s  => IdEx_Shift_Sel(0),
              o  => s_ShamtMux);

    ShamtMux1: structural_mux_n_bit
      generic map (N => 5)
      port map(i0 => s_ShamtMux,
               i1 => "10000",
               s  => IdEx_Shift_Sel(1),
               o  => s_Shamt);
			   
	RtRdMux: structural_mux_n_bit
      generic map (N => 5)
      port map (i0 => IdEx_rt,
                i1 => IdEx_rd,
                s  => IdEx_RegDst,
                o  => s_RtRd_Carry0);
				
	Wa31Mux: structural_mux_n_bit
	  generic map (N => 5)
      port map (i0 => s_RtRd_Carry0,
                i1 => "11111",
                s  => IdEx_JAL,
				o  => s_RtRd_Carry1);
                --o  => s_RegWrAddr);
				
	ForwardUnit: forward_unit
	   port map(
	    IdEx_op        => IdEx_op,
		IdEx_func      => IdEx_func,
		IdEx_s_RegWr   => IdEx_s_RegWr,
		IdEx_rd        => IdEx_rd,
		IdEx_rs        => IdEx_rs,
		IdEx_rt        => IdEx_rt,
		
	    ExMem_op       => ExMem_op,
		ExMem_func     => ExMem_func,
		ExMem_s_RegWr  => ExMem_s_RegWr ,
		ExMem_rd       => ExMem_rd,
		ExMem_rs       => ExMem_rs,
		ExMem_rt       => ExMem_rt,
		
	    MemWb_op       => MemWb_op,
		MemWb_func     => MemWb_func,
		MemWb_s_RegWr  => MemWb_s_RegWr,
		MemWb_rd       => MemWb_rd,
		MemWb_rs       => MemWb_rs,
		MemWb_rt       => MemWb_rt,
		
		ForwardA       => ForwardA,
		ForwardB       => ForwardB,
		ForwardC       => ForwardC);
				
	ExMemRegister: EXMEM
	  port map( clock         => iCLK,
				flush         => ExMem_FLUSH or iRST,
				stall         => ExMem_STALL,
       
				i_JAL         => IdEx_JAL,

				i_MemToReg    => IdEx_MemToReg,
				i_s_DMemWr    => IdEx_s_DMemWr,
				i_s_RegWr     => IdEx_s_RegWr,
	   
				i_ALUResult   => ALUResult,
				i_d1          => s_ForwardC1,
				i_destination => s_RtRd_Carry1,
		
				i_op          => IdEx_op,
				i_func        => IdEx_func,
				
				i_PcAddress   => IdEx_PcAddress,

				i_rs          => IdEx_rs,
				i_rt          => IdEx_rt,
				i_rd          => IdEx_rd,
		
				o_JAL         => ExMem_JAL,

				o_MemToReg    => ExMem_MemToReg,
				o_s_DMemWr    => ExMem_s_DMemWr,
				o_s_RegWr     => ExMem_s_RegWr,
       
				o_ALUResult   => ExMem_ALUResult,
				o_d1          => ExMem_d1,
				o_destination => ExMem_destination,
		
				o_op          => ExMem_op,
				o_func        => ExMem_func,
				
				o_PcAddress   => ExMem_PcAddress,

				o_rs          => ExMem_rs,
				o_rt          => ExMem_rt,
				o_rd          => ExMem_rd);		

	s_DMemWrAssign: s_DMemWr <= ExMem_s_DMemWr;				

	MEMWBRegister: MEMWB port map(
        clock         => iCLK,
		flush         => MemWb_FLUSH or iRST,
		stall         => MemWb_STALL,
		
		i_op          => ExMem_op,
		i_func        => ExMem_func,
		
		i_PcAddress   => ExMem_PcAddress,
		
		i_JAL         => ExMem_JAL,
		
		i_MemToReg    => ExMem_MemToReg,
		i_s_RegWr     => ExMem_s_RegWr,
		i_d1          => ExMem_d1,
		i_ALUResult   => ExMem_ALUResult,
		i_MemOut      => s_DMemOut,
		i_destination => ExMem_destination,
		i_rs          => ExMem_rs,
		i_rt          => ExMem_rt,
		i_rd          => ExMem_rd,		
		
		o_op          => MemWb_op,
		o_func        => MemWb_func,
		
		o_PcAddress   => MemWb_PcAddress,
		
		o_JAL         => MemWb_JAL,
		
		o_MemToReg    => MemWb_MemToReg,
		o_s_RegWr     => MemWb_s_RegWr,
		o_d1          => MemWb_d1,
		o_ALUResult   => MemWb_ALUResult,
		o_MemOut      => MemWb_MemOut,
		o_destination => MemWb_destination,
		o_rs          => MemWb_rs,
		o_rt          => MemWb_rt,
		o_rd          => MemWb_rd);
				
	s_Halt <='1' when (MemWb_op = "000000") and (MemWb_func = "001100") and (v0 = "00000000000000000000000000001010") else '0';
	
	s_RegWrAssign: s_RegWr <= MemWb_s_RegWr;
	
	s_RegWrAddrAssign: s_RegWrAddr <= MemWb_destination;
	
	s_DMemAddrAssign: s_DMemAddr <= ExMem_ALUResult;
	
	DMemDataInput: s_DMemData <= ExMem_d1;

    DMemAddressInput: oALUOut <= s_DMemAddr;

    AluMemMux: structural_mux_n_bit
      generic map (N => 32)
      port map(i0 => MemWb_ALUResult,
               i1 => MemWb_MemOut,
               s  => MemWb_MemToReg,
               o  => s_AluMem);

end structure;
