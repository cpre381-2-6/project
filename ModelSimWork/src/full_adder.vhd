library IEEE;
use IEEE.std_logic_1164.all;

entity full_adder is
   port(A    : in std_logic;
	B    : in std_logic;
	Cin  : in std_logic;
	Sum  : out std_logic;
        Cout : out std_logic);

end full_adder;

architecture dataflow of full_adder is

signal BxorC : std_logic;

signal w : std_logic;
signal x : std_logic;
signal y : std_logic;
signal z : std_logic;


begin

--l0: xorg2 port map(B, Cin, BxorC);
l0: BxorC <= B xor Cin;

--l1: xorg2 port map(A, BxorC, Sum);
l1: Sum <= A xor BxorC;

--l2: andg2 port map(B, Cin, w);
l2: w <= B and Cin;

--l3: andg2 port map(A, B, x);
l3: x <= A and B;

--l4: andg2 port map(A, Cin, y);
l4: y <= A and Cin;

--l5: org2 port map(w, x, z);
l5: z <= w or x;

--l6: org2 port map(z, y, Cout);
l6: Cout <= z or y;

end dataflow;