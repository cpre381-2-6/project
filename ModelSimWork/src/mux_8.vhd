library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux_8 is
    port ( sel  : in  STD_LOGIC_VECTOR (2 downto 0);
           i0   : in  STD_LOGIC;
           i1   : in  STD_LOGIC;
           i2   : in  STD_LOGIC;
           i3   : in  STD_LOGIC;
           i4   : in  STD_LOGIC;
           i5   : in  STD_LOGIC;
           i6   : in  STD_LOGIC;
           i7   : in  STD_LOGIC;
           o    : out STD_LOGIC);
end mux_8;                   

architecture Behavioral of mux_8 is
begin
process (sel, i0, i1, i2, i3, i4, i5, i6, i7)
begin

    case sel is
        when "000"   => o <= i0;
        when "001"   => o <= i1;
        when "010"   => o <= i2;
        when "011"   => o <= i3;
        when "100"   => o <= i4;
        when "101"   => o <= i5;
        when "110"   => o <= i6;
	when others  => o <= i7;         
    end case;

end process;
end Behavioral;