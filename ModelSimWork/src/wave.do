onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_pipeline_reg/s_CLK
add wave -noupdate /tb_pipeline_reg/IfId_FLUSH
add wave -noupdate /tb_pipeline_reg/IfId_STALL
add wave -noupdate /tb_pipeline_reg/instruction
add wave -noupdate /tb_pipeline_reg/PcAddress
add wave -noupdate /tb_pipeline_reg/IfIdInstruction
add wave -noupdate /tb_pipeline_reg/IfIdPcAddress
add wave -noupdate /tb_pipeline_reg/IdEx_FLUSH
add wave -noupdate /tb_pipeline_reg/IdEx_STALL
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ALUorBS
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ALUSrc
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/Arithmetic
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/BNE
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/Branch
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/J
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/JAL
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/JR
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/MemToReg
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/RegDst
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/s_DMemWr
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/s_RegWr
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/Shift_L
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/Unsign
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_ALUorBS
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_ALUSrc
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_Arithmetic
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_BNE
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_Branch
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_J
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_JAL
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_JR
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_MemToReg
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_RegDst
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_s_DMemWr
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_s_RegWr
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_Shift_L
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_Unsign
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ALUControl
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_ALUControl
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/Shift_Sel
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_Shift_Sel
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/d0
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/d1
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_d0
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/IdEx_d1
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/zero
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ALUResult
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/destination
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_BNE
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_Branch
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_J
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_JAL
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_JR
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_MemToReg
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_s_DMemWr
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_s_RegWr
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_zero
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_ALUResult
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_d1
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/ExMem_destination
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/MemWb_MemToReg
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/MemWb_s_RegWr
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/MemWb_ALUResult
add wave -noupdate -group UNCONNECTED /tb_pipeline_reg/MemWb_destination
add wave -noupdate /tb_pipeline_reg/IdEx_PcAddress
add wave -noupdate /tb_pipeline_reg/IdEx_extended
add wave -noupdate /tb_pipeline_reg/IdEx_rs
add wave -noupdate /tb_pipeline_reg/IdEx_rt
add wave -noupdate /tb_pipeline_reg/IdEx_rd
add wave -noupdate /tb_pipeline_reg/IdEx_shamt
add wave -noupdate /tb_pipeline_reg/ExMem_FLUSH
add wave -noupdate /tb_pipeline_reg/ExMem_STALL
add wave -noupdate /tb_pipeline_reg/ExMem_PcAddress
add wave -noupdate /tb_pipeline_reg/ExMem_rs
add wave -noupdate /tb_pipeline_reg/ExMem_rt
add wave -noupdate /tb_pipeline_reg/ExMem_rd
add wave -noupdate /tb_pipeline_reg/MemWb_FLUSH
add wave -noupdate /tb_pipeline_reg/MemWb_STALL
add wave -noupdate /tb_pipeline_reg/MemWb_rs
add wave -noupdate /tb_pipeline_reg/MemWb_rt
add wave -noupdate /tb_pipeline_reg/MemWb_rd
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 0
configure wave -namecolwidth 254
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {925 ns}
