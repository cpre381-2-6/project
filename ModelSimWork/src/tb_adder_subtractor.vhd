library IEEE;
use IEEE.std_logic_1164.all;

-- This is an empty entity so we don't need to declare ports
entity tb_adder_subtractor is

end tb_adder_subtractor;

architecture behavior of tb_adder_subtractor is

component adder_subtractor
   generic(N : integer := 32);
   port(A        : in std_logic_vector(N-1 downto 0);
	B        : in std_logic_vector(N-1 downto 0);
	nAdd_Sub : in std_logic;
	S        : out std_logic_vector(N-1 downto 0);
        Cout     : out std_logic);
end component;

signal nAdd_Sub, Cout : std_logic;
signal A, B, S        : std_logic_vector(31 downto 0);

begin

DUT0: adder_subtractor port map(A, B, nAdd_Sub, S, Cout);

  process
  begin

    A        <= x"00000000";
    B        <= x"10101010";
    nAdd_Sub <= '1';
    wait for 100 ns;

    A        <= x"11111111";
    B        <= x"10000000";
    nAdd_Sub <= '1';
    wait for 100 ns;

    A        <= x"01111111";
    B        <= x"00000000";
    nAdd_Sub <= '1';
    wait for 100 ns;

    A        <= x"01010101";
    B        <= x"10101010";
    nAdd_Sub <= '1';
    wait for 100 ns;

  end process;

end behavior;