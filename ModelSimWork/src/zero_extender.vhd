library IEEE;
use IEEE.std_logic_1164.all;

entity zero_extender is
   port(input  : in std_logic_vector(15 downto 0);
        output : out std_logic_vector(31 downto 0));

end zero_extender;

architecture dataflow of zero_extender is

begin

l0:  output(0)  <= input(0);
l1:  output(1)  <= input(1);
l2:  output(2)  <= input(2);
l3:  output(3)  <= input(3);
l4:  output(4)  <= input(4);
l5:  output(5)  <= input(5);
l6:  output(6)  <= input(6);
l7:  output(7)  <= input(7);
l8:  output(8)  <= input(8);
l9:  output(9)  <= input(9);
l10: output(10) <= input(10);
l11: output(11) <= input(11);
l12: output(12) <= input(12);
l13: output(13) <= input(13);
l14: output(14) <= input(14);
l15: output(15) <= input(15);

l16: output(16) <= '0';
l17: output(17) <= '0';
l18: output(18) <= '0';
l19: output(19) <= '0';
l20: output(20) <= '0';
l21: output(21) <= '0';
l22: output(22) <= '0';
l23: output(23) <= '0';
l24: output(24) <= '0';
l25: output(25) <= '0';
l26: output(26) <= '0';
l27: output(27) <= '0';
l28: output(28) <= '0';
l29: output(29) <= '0';
l30: output(30) <= '0';
l31: output(31) <= '0';

end dataflow;