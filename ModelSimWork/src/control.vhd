library IEEE;
use IEEE.std_logic_1164.all;

entity control is
  port( func : in STD_LOGIC_VECTOR(5 downto 0);
	op   : in STD_LOGIC_VECTOR(5 downto 0);
	ALUControl : out STD_LOGIC_VECTOR(2 downto 0);
	ALUorBS    : out STD_LOGIC;
	ALUSrc     : out STD_LOGIC;
	Arithmetic : out STD_LOGIC;
	BNE        : out STD_LOGIC;
	Branch     : out STD_LOGIC;
	J          : out STD_LOGIC;
	JAL        : out STD_LOGIC;
	JR         : out STD_LOGIC;
	MemToReg   : out STD_LOGIC;
	RegDst     : out STD_LOGIC;
	s_DMemWr   : out STD_LOGIC;
	s_RegWr    : out STD_LOGIC;
	Shift_L    : out STD_LOGIC;
	Shift_Sel  : out STD_LOGIC_VECTOR(1 downto 0);
	Unsign     : out STD_LOGIC;
	Zero_Ext   : out STD_LOGIC);
end control;

architecture structure of control is

begin

  p_CASE : process (func, op)

  begin
  
	ALUControl <= "000";
	ALUorBS    <= '0';
	ALUSrc     <= '0';
	Arithmetic <= '0';
	BNE        <= '0';
	Branch     <= '0';
	J          <= '0';
	JAL        <= '0';
	JR         <= '0';
	MemToReg   <= '0';
	RegDst     <= '0';
	s_DMemWr   <= '0';
	s_RegWr    <= '0';
	Shift_L    <= '0';
	Shift_Sel  <= "00";
	Unsign     <= '0';
	Zero_Ext   <= '0';

	case op is
      	when "000000" =>
      		case func is
      		when "100000" => --add
      			ALUControl <= "101";
      			RegDst     <= '1';
      			s_RegWr    <= '1';
      		when "100001" => --addu
      			ALUControl <= "101";
      			RegDst     <= '1';
      			s_RegWr    <= '1';
      		when "100100" => --and
      			ALUControl <= "001";
      			RegDst     <= '1';
      			s_RegWr    <= '1';
      		when "100111" => --nor
      			ALUControl <= "100";
      			RegDst     <= '1';
      			s_RegWr    <= '1';
      		when "100110" => --xor
      			ALUControl <= "010";
      			RegDst     <= '1';
      			s_RegWr    <= '1';
      		when "100101" => --or
      			RegDst     <= '1';
      			s_RegWr    <= '1';
          when "101010" => --slt
      			ALUControl <= "110";
      			RegDst     <= '1';
      			s_RegWr    <= '1';
          when "101011" => --sltu
      			ALUControl <= "110";
      			RegDst     <= '1';
      			s_RegWr    <= '1';
      			Unsign     <= '1';
          when "000000" => --sll
      			ALUorBS    <= '1';
      			RegDst     <= '1';
      			s_RegWr    <= '1';
      			Shift_L    <= '1';
          when "000010" => --srl
      			ALUorBS    <= '1';
      			RegDst     <= '1';
      			s_RegWr    <= '1';
          when "000011" => --sra
      			ALUorBS    <= '1';
      			Arithmetic <= '1';
      			RegDst     <= '1';
      			s_RegWr    <= '1';
          when "000100" => --sllv
      			ALUorBS    <= '1';
      			RegDst     <= '1';
      			s_RegWr    <= '1';
      			Shift_L    <= '1';
      			Shift_Sel  <= "01";
          when "000110" => --srlv
      			ALUorBS    <= '1';
      			RegDst     <= '1';
      			s_RegWr    <= '1';
      			Shift_Sel  <= "01";
          when "000111" => --srav
      			ALUorBS    <= '1';
      			Arithmetic <= '1';
      			RegDst     <= '1';
      			s_RegWr    <= '1';
      			Shift_Sel  <= "01";
          when "100010" => --sub
      			ALUControl <= "111";
      			RegDst     <= '1';
      			s_RegWr    <= '1';
          when "100011" => --subu
      			ALUControl <= "111";
      			RegDst     <= '1';
      			s_RegWr    <= '1';
		  when "001000" => --jr
				J          <= '1';
				JR         <= '1';
      	  when others =>

      		end case;
      	when "001000" => --addi
      		ALUControl <= "101";
      		ALUSrc     <= '1';
      		s_RegWr    <= '1';
      	when "001001" => --addiu
      		ALUControl <= "101";
      		ALUSrc     <= '1';
      		s_RegWr    <= '1';
      	when "001100" => --andi
      		ALUControl <= "001";
      		ALUSrc     <= '1';
      		s_RegWr    <= '1';
			Zero_Ext   <= '1';
      	when "001111" => --lui
      		ALUorBS    <= '1';
      		ALUSrc     <= '1';
      		s_RegWr    <= '1';
      		Shift_L    <= '1';
      		Shift_Sel  <= "10";
      	when "100011" => --lw
      		ALUControl <= "101";
      		ALUSrc     <= '1';
      		MemToReg   <= '1';
      		s_RegWr    <= '1';
      	when "001110" => --xori
      		ALUControl <= "010";
      		ALUSrc     <= '1';
      		s_RegWr    <= '1';
			Zero_Ext   <= '1';
      	when "001101" => --ori
      		ALUSrc     <= '1';
      		s_RegWr    <= '1';
			Zero_Ext   <= '1';
        when "001010" => --slti
      		ALUControl <= "110";
      		ALUSrc     <= '1';
      		s_RegWr    <= '1';
        when "001011" => --sltiu
      		ALUControl <= "110";
      		ALUSrc     <= '1';
      		s_RegWr    <= '1';
      		Unsign     <= '1';
        when "101011" => --sw
      		ALUControl <= "101";
      		ALUSrc     <= '1';
      		s_DMemWr   <= '1';
		when "000100" => --beq
			ALUControl <= "111";
			Branch     <= '1';
		when "000101" => --bne
			ALUControl <= "111";
			BNE        <= '1';
			Branch     <= '1';		
		when "000010" => --j
			J          <= '1';
		when "000011" => --jal
			J          <= '1';
			JAL        <= '1';
			s_RegWr    <= '1';
      	when others =>

    	end case;

  end process;

end structure;
