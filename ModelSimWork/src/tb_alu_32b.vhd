library IEEE;
use IEEE.std_logic_1164.all;

entity tb_alu_32b is

end tb_alu_32b;

architecture behavior of tb_alu_32b is

component alu_32b
   port(A        : in std_logic_vector(31 downto 0);
	B        : in std_logic_vector(31 downto 0);
	Control  : in std_logic_vector(2 downto 0);
	Result   : out std_logic_vector(31 downto 0);
	Carryout : out std_logic;
	Overflow : out std_logic;
	Zero     : out std_logic);
end component;

signal Carryout, Overflow, Zero : std_logic;
signal Control                  : std_logic_vector(2 downto 0);
signal A, B, Result             : std_logic_vector(31 downto 0);

begin

DUT0: alu_32b port map(A, B, Control, Result, Carryout, Overflow, Zero);

  process
  begin

	-- OR
	A <= "00000000000000000000000000101010";
	B <= "00000000000000000000000001010101";
	Control <= "000";
    	wait for 100 ns;

	-- AND
	A <= "00000000000000000000000000101011";
	B <= "00000000000000000000000001011101";
	Control <= "001";
    	wait for 100 ns;

	-- XOR
	A <= "00000000000000000000000001101111";
	B <= "00000000000000000000000001110101";
	Control <= "010";
    	wait for 100 ns;

	-- NAND
	A <= "00000000000000000000000001101111";
	B <= "00000000000000000000000001010101";
	Control <= "011";
    	wait for 100 ns;

	-- NOR
	A <= "00000000000000000000000000101010";
	B <= "00000000000000000000000001010101";
	Control <= "100";
    	wait for 100 ns;

	-- ADD
	A <= "00000000000000000000000000101011"; -- 43
	B <= "00000000000000000000000001010101"; -- 85
	Control <= "101";
    	wait for 100 ns;

	-- SLT
	A <= "00000000000000000000001000110111"; -- 567
	B <= "10000000000000000000000001100011"; -- -2147483549
	Control <= "110";
    	wait for 100 ns;

	-- SLT
	A <= "00000000000000000000000001100011"; -- 99
	B <= "00000000000000000000001000110111"; -- 567
	Control <= "110";
    	wait for 100 ns;

	-- SUB
	A <= "00000000000000000000001000110111"; -- 567
	B <= "00000000000000000000000001100011"; -- 99
	Control <= "111";
    	wait for 100 ns;

  end process;

end behavior;