library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux_2 is
    port ( sel  : in  STD_LOGIC;
           i0   : in  STD_LOGIC;
           i1   : in  STD_LOGIC;
           o    : out STD_LOGIC);
end mux_2;                   

architecture Behavioral of mux_2 is
begin
process (sel, i0, i1)
begin

    case sel is
        when '0'    => o <= i0;
	when others => o <= i1;         
    end case;

end process;
end Behavioral;