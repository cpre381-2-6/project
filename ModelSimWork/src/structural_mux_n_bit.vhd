library IEEE;
use IEEE.std_logic_1164.all;

entity structural_mux_n_bit is
   generic(N : integer := 32);
   port(i0  : in std_logic_vector(N-1 downto 0);
	i1  : in std_logic_vector(N-1 downto 0);
	s   : in std_logic;
        o   : out std_logic_vector(N-1 downto 0));

end structural_mux_n_bit;

architecture structure of structural_mux_n_bit is

component invg
  port(i_A  : in std_logic;
       o_F  : out std_logic);
end component;

component andg2
  port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
end component;

component org2
  port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
end component;

signal not_s : std_logic;
signal x : std_logic_vector(N-1 downto 0);
signal y : std_logic_vector(N-1 downto 0);

begin

l0: invg port map(s, not_s);

l1: for i in 0 to N-1 generate
  andg2_i: andg2 port map(i0(i), not_s, x(i));
end generate;

l2: for i in 0 to N-1 generate
  andg2_i: andg2 port map(i1(i), s, y(i));
end generate;

l3: for i in 0 to N-1 generate
  org2_i: org2 port map(x(i), y(i), o(i));
end generate;

end structure;