library IEEE;
use IEEE.std_logic_1164.all;

entity EXMEM is
  port(	clock        : in std_logic;
		flush        : in std_logic;
		stall        : in std_logic;
       
		i_JAL        : in STD_LOGIC;

		i_MemToReg   : in STD_LOGIC;
		i_s_DMemWr   : in STD_LOGIC;
		i_s_RegWr    : in STD_LOGIC;
	   
		i_ALUResult   : in std_logic_vector(31 downto 0);
		i_d1          : in std_logic_vector(31 downto 0);
		i_destination : in std_logic_vector(4 downto 0);
		
		i_op         : in STD_LOGIC_VECTOR(5 downto 0);
		i_func       : in STD_LOGIC_VECTOR(5 downto 0);	
		
		i_PcAddress  : in std_logic_vector(31 downto 0);

		i_rs         : in std_logic_vector(4 downto 0);
		i_rt         : in std_logic_vector(4 downto 0);
		i_rd         : in std_logic_vector(4 downto 0);	
		
		o_JAL        : out STD_LOGIC;

		o_MemToReg   : out STD_LOGIC;
		o_s_DMemWr   : out STD_LOGIC;
		o_s_RegWr    : out STD_LOGIC;
       
		o_ALUResult   : out std_logic_vector(31 downto 0);
		o_d1          : out std_logic_vector(31 downto 0);
		o_destination : out std_logic_vector(4 downto 0);
		
	    o_op         : out STD_LOGIC_VECTOR(5 downto 0);
		o_func       : out STD_LOGIC_VECTOR(5 downto 0);
		
		o_PcAddress  : out std_logic_vector(31 downto 0);

		o_rs         : out std_logic_vector(4 downto 0);
		o_rt         : out std_logic_vector(4 downto 0);
		o_rd         : out std_logic_vector(4 downto 0));
end EXMEM;

architecture structure of EXMEM is

signal not_stall : std_logic;

component dflipf
  port(i_CLK        : in std_logic;     
       i_RST        : in std_logic;     
       i_WE         : in std_logic;     
       i_D          : in std_logic;     
       o_Q          : out std_logic);   
end component;

component n_bit_register
  generic(N : integer := 32);
  port(i_CLK        : in std_logic;
       i_RST        : in std_logic;
       i_WE         : in std_logic;
       i_D          : in std_logic_vector(N-1 downto 0);
       o_Q          : out std_logic_vector(N-1 downto 0));
end component;

begin

StallSignal: not_stall <= not stall;
		
JALRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_JAL,
		o_Q   => o_JAL);
		
MemToRegRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_MemToReg,
		o_Q   => o_MemToReg);
		
s_DMemWrRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_s_DMemWr,
		o_Q   => o_s_DMemWr);
		
s_RegWrRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_s_RegWr,
		o_Q   => o_s_RegWr);
		
ALUResultRegister: n_bit_register
	generic map (N => 32)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_ALUResult,
		o_Q   => o_ALUResult);
		
d1Register: n_bit_register
	generic map (N => 32)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_d1,
		o_Q   => o_d1);
		
destinationRegister: n_bit_register
	generic map (N => 5)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_destination,
		o_Q   => o_destination);
		
OpRegister: n_bit_register
	generic map (N => 6)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_op,
		o_Q   => o_op);
		
FuncRegister: n_bit_register
	generic map (N => 6)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_func,
		o_Q   => o_func);
		
PcAddressRegister: n_bit_register
	generic map (N => 32)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_PcAddress,
		o_Q   => o_PcAddress);
		
rsRegister: n_bit_register
	generic map (N => 5)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_rs,
		o_Q   => o_rs);
		
		
rtRegister: n_bit_register
	generic map (N => 5)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_rt,
		o_Q   => o_rt);
		
rdRegister: n_bit_register
	generic map (N => 5)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_rd,
		o_Q   => o_rd);

end structure;