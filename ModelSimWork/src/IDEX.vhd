library IEEE;
use IEEE.std_logic_1164.all;

entity IDEX is
  port(	clock        : in std_logic;
		flush        : in std_logic;
		stall        : in std_logic;
       
		i_ALUControl : in STD_LOGIC_VECTOR(2 downto 0);
		i_ALUorBS    : in STD_LOGIC;
		i_ALUSrc     : in STD_LOGIC;
		i_Arithmetic : in STD_LOGIC;

		i_JAL        : in STD_LOGIC;

		i_MemToReg   : in STD_LOGIC;
		i_RegDst     : in STD_LOGIC;
		i_s_DMemWr   : in STD_LOGIC;
		i_s_RegWr    : in STD_LOGIC;
		i_Shift_L    : in STD_LOGIC;
		i_Shift_Sel  : in STD_LOGIC_VECTOR(1 downto 0);
		i_Unsign     : in STD_LOGIC;
	   
	    i_op         : in STD_LOGIC_VECTOR(5 downto 0);
		i_func       : in STD_LOGIC_VECTOR(5 downto 0);
		
		i_PcAddress  : in std_logic_vector(31 downto 0);
	   
		i_d0		 : in std_logic_vector(31 downto 0);
		i_d1		 : in std_logic_vector(31 downto 0);
		i_extended   : in std_logic_vector(31 downto 0);
		i_rs         : in std_logic_vector(4 downto 0);
		i_rt         : in std_logic_vector(4 downto 0);
		i_rd         : in std_logic_vector(4 downto 0);
		i_shamt      : in std_logic_vector(4 downto 0);
		
		o_ALUControl : out STD_LOGIC_VECTOR(2 downto 0);
		o_ALUorBS    : out STD_LOGIC;
		o_ALUSrc     : out STD_LOGIC;
		o_Arithmetic : out STD_LOGIC;

		o_JAL        : out STD_LOGIC;

		o_MemToReg   : out STD_LOGIC;
		o_RegDst     : out STD_LOGIC;
		o_s_DMemWr   : out STD_LOGIC;
		o_s_RegWr    : out STD_LOGIC;
		o_Shift_L    : out STD_LOGIC;
		o_Shift_Sel  : out STD_LOGIC_VECTOR(1 downto 0);
		o_Unsign     : out STD_LOGIC;
		
	    o_op         : out STD_LOGIC_VECTOR(5 downto 0);
		o_func       : out STD_LOGIC_VECTOR(5 downto 0);
		
		o_PcAddress  : out std_logic_vector(31 downto 0);
       
		o_d0         : out std_logic_vector(31 downto 0);
		o_d1		 : out std_logic_vector(31 downto 0);
		o_extended   : out std_logic_vector(31 downto 0);
		o_rs         : out std_logic_vector(4 downto 0);
		o_rt         : out std_logic_vector(4 downto 0);
		o_rd         : out std_logic_vector(4 downto 0);
		o_shamt      : out std_logic_vector(4 downto 0));
end IDEX;

architecture structure of IDEX is

signal not_stall : std_logic;

component dflipf
  port(i_CLK        : in std_logic;     
       i_RST        : in std_logic;     
       i_WE         : in std_logic;     
       i_D          : in std_logic;     
       o_Q          : out std_logic);   
end component;

component n_bit_register
  generic(N : integer := 32);
  port(i_CLK        : in std_logic;
       i_RST        : in std_logic;
       i_WE         : in std_logic;
       i_D          : in std_logic_vector(N-1 downto 0);
       o_Q          : out std_logic_vector(N-1 downto 0));
end component;

begin

StallSignal: not_stall <= not stall;

ALUControlRegister: n_bit_register
	generic map (N => 3)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_ALUControl,
		o_Q   => o_ALUControl);
		
ALUorBSRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_ALUorBS,
		o_Q   => o_ALUorBS);
		
ALUSrcRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_ALUSrc,
		o_Q   => o_ALUSrc);
		
ArithmeticRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_Arithmetic,
		o_Q   => o_Arithmetic);
		
JALRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_JAL,
		o_Q   => o_JAL);
		
MemToRegRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_MemToReg,
		o_Q   => o_MemToReg);
		
RegDstRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_RegDst,
		o_Q   => o_RegDst);
		
s_DMemWrRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_s_DMemWr,
		o_Q   => o_s_DMemWr);
		
s_RegWrRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_s_RegWr,
		o_Q   => o_s_RegWr);
		
Shift_LRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_Shift_L,
		o_Q   => o_Shift_L);
		
Shift_SelRegister: n_bit_register
	generic map (N => 2)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_Shift_Sel,
		o_Q   => o_Shift_Sel);
		
UnsignRegister: dflipf
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_Unsign,
		o_Q   => o_Unsign);
		
OpRegister: n_bit_register
	generic map (N => 6)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_op,
		o_Q   => o_op);
		
FuncRegister: n_bit_register
	generic map (N => 6)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_func,
		o_Q   => o_func);
		
PcAddressRegister: n_bit_register
	generic map (N => 32)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_PcAddress,
		o_Q   => o_PcAddress);
		
d0Register: n_bit_register
	generic map (N => 32)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_d0,
		o_Q   => o_d0);
		
d1Register: n_bit_register
	generic map (N => 32)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_d1,
		o_Q   => o_d1);
		
extendedRegister: n_bit_register
	generic map (N => 32)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_extended,
		o_Q   => o_extended);
		
rsRegister: n_bit_register
	generic map (N => 5)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_rs,
		o_Q   => o_rs);
		
		
rtRegister: n_bit_register
	generic map (N => 5)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_rt,
		o_Q   => o_rt);
		
rdRegister: n_bit_register
	generic map (N => 5)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_rd,
		o_Q   => o_rd);
		
shamtRegister: n_bit_register
	generic map (N => 5)
	port map (
		i_CLK => clock,
		i_RST => flush,
		i_WE  => not_stall,
		i_D   => i_shamt,
		o_Q   => o_shamt);

end structure;