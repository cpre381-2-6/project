
#
# CprE 381 toolflow Timing dump
#

FMax: 27.47mhz Clk Constraint: 20.00ns Slack: -16.40ns

The path is given below

 ===================================================================
 From Node    : n_bit_register:PC|dflipf:\l0:5:dff_i|s_Q
 To Node      : register_file:RegisterFile|n_bit_register:\line2:23:register_i|dflipf:\l0:28:dff_i|s_Q
 Launch Clock : iCLK
 Latch Clock  : iCLK
 Data Arrival Path:
 Total (ns)  Incr (ns)     Type  Element
 ==========  ========= ==  ====  ===================================
      0.000      0.000           launch edge time
      3.484      3.484  R        clock network delay
      3.716      0.232     uTco  n_bit_register:PC|dflipf:\l0:5:dff_i|s_Q
      3.716      0.000 FF  CELL  PC|\l0:5:dff_i|s_Q|q
      4.064      0.348 FF    IC  s_IMemAddr[5]~6|datad
      4.189      0.125 FF  CELL  s_IMemAddr[5]~6|combout
      6.948      2.759 FF    IC  IMem|ram~44475|datab
      7.371      0.423 FR  CELL  IMem|ram~44475|combout
      8.660      1.289 RR    IC  IMem|ram~44476|datad
      8.815      0.155 RR  CELL  IMem|ram~44476|combout
      9.846      1.031 RR    IC  IMem|ram~44477|datad
     10.001      0.155 RR  CELL  IMem|ram~44477|combout
     10.205      0.204 RR    IC  IMem|ram~44478|datad
     10.360      0.155 RR  CELL  IMem|ram~44478|combout
     10.564      0.204 RR    IC  IMem|ram~44489|datac
     10.834      0.270 RF  CELL  IMem|ram~44489|combout
     11.059      0.225 FF    IC  IMem|ram~44532|datad
     11.184      0.125 FF  CELL  IMem|ram~44532|combout
     13.040      1.856 FF    IC  IMem|ram~44575|datac
     13.321      0.281 FF  CELL  IMem|ram~44575|combout
     13.548      0.227 FF    IC  IMem|ram~45087|datad
     13.673      0.125 FF  CELL  IMem|ram~45087|combout
     15.390      1.717 FF    IC  RegisterFile|line4|Mux28~9|datab
     15.813      0.423 FR  CELL  RegisterFile|line4|Mux28~9|combout
     16.016      0.203 RR    IC  RegisterFile|line4|Mux28~10|datad
     16.171      0.155 RR  CELL  RegisterFile|line4|Mux28~10|combout
     17.457      1.286 RR    IC  RegisterFile|line4|Mux28~11|datac
     17.742      0.285 RR  CELL  RegisterFile|line4|Mux28~11|combout
     18.996      1.254 RR    IC  RegisterFile|line4|Mux28~16|datac
     19.283      0.287 RR  CELL  RegisterFile|line4|Mux28~16|combout
     19.488      0.205 RR    IC  RegisterFile|line4|Mux28~19|datad
     19.627      0.139 RF  CELL  RegisterFile|line4|Mux28~19|combout
     19.875      0.248 FF    IC  AluBMux|\l3:3:org2_i|o_F~0|datad
     20.000      0.125 FF  CELL  AluBMux|\l3:3:org2_i|o_F~0|combout
     20.254      0.254 FF    IC  AluComponent|bs|\col_0_muxes:27:col_0_i|o~1|datac
     20.535      0.281 FF  CELL  AluComponent|bs|\col_0_muxes:27:col_0_i|o~1|combout
     20.826      0.291 FF    IC  AluComponent|bs|\col_0_muxes:28:col_0_i|o~1|datab
     21.251      0.425 FF  CELL  AluComponent|bs|\col_0_muxes:28:col_0_i|o~1|combout
     21.987      0.736 FF    IC  AluComponent|bs|\col_2_muxes_0:26:col_2_i_0|o~0|datac
     22.268      0.281 FF  CELL  AluComponent|bs|\col_2_muxes_0:26:col_2_i_0|o~0|combout
     23.194      0.926 FF    IC  AluComponent|bs|\col_2_muxes_0:22:col_2_i_0|o~1|datad
     23.319      0.125 FF  CELL  AluComponent|bs|\col_2_muxes_0:22:col_2_i_0|o~1|combout
     23.569      0.250 FF    IC  AluComponent|bs|\col_3_muxes_0:22:col_3_i_0|o~2|datad
     23.694      0.125 FF  CELL  AluComponent|bs|\col_3_muxes_0:22:col_3_i_0|o~2|combout
     24.618      0.924 FF    IC  AluComponent|mux|\l3:22:org2_i|o_F~4|datad
     24.743      0.125 FF  CELL  AluComponent|mux|\l3:22:org2_i|o_F~4|combout
     24.980      0.237 FF    IC  AluComponent|mux|\l3:9:org2_i|o_F~0|datad
     25.105      0.125 FF  CELL  AluComponent|mux|\l3:9:org2_i|o_F~0|combout
     25.333      0.228 FF    IC  AluComponent|mux|\l3:9:org2_i|o_F~1|datad
     25.458      0.125 FF  CELL  AluComponent|mux|\l3:9:org2_i|o_F~1|combout
     28.183      2.725 FF    IC  DMem|ram~53490|dataa
     28.595      0.412 FR  CELL  DMem|ram~53490|combout
     28.799      0.204 RR    IC  DMem|ram~53491|datad
     28.954      0.155 RR  CELL  DMem|ram~53491|combout
     30.533      1.579 RR    IC  DMem|ram~53499|datac
     30.820      0.287 RR  CELL  DMem|ram~53499|combout
     33.817      2.997 RR    IC  DMem|ram~53510|dataa
     34.214      0.397 RR  CELL  DMem|ram~53510|combout
     34.448      0.234 RR    IC  DMem|ram~53521|datab
     34.882      0.434 RF  CELL  DMem|ram~53521|combout
     35.109      0.227 FF    IC  DMem|ram~53564|datad
     35.234      0.125 FF  CELL  DMem|ram~53564|combout
     35.502      0.268 FF    IC  DMem|ram~53607|datab
     35.906      0.404 FF  CELL  DMem|ram~53607|combout
     36.133      0.227 FF    IC  DMem|ram~53778|datad
     36.258      0.125 FF  CELL  DMem|ram~53778|combout
     36.487      0.229 FF    IC  DMem|ram~53949|datad
     36.637      0.150 FR  CELL  DMem|ram~53949|combout
     36.842      0.205 RR    IC  WdMux|\l3:28:org2_i|o_F~0|datad
     36.997      0.155 RR  CELL  WdMux|\l3:28:org2_i|o_F~0|combout
     37.202      0.205 RR    IC  WdMux|\l3:28:org2_i|o_F~1|datad
     37.357      0.155 RR  CELL  WdMux|\l3:28:org2_i|o_F~1|combout
     39.403      2.046 RR    IC  RegisterFile|\line2:23:register_i|\l0:28:dff_i|s_Q|asdata
     39.809      0.406 RR  CELL  register_file:RegisterFile|n_bit_register:\line2:23:register_i|dflipf:\l0:28:dff_i|s_Q
 Data Required Path:
 Total (ns)  Incr (ns)     Type  Element
 ==========  ========= ==  ====  ===================================
     20.000     20.000           latch edge time
     23.401      3.401  R        clock network delay
     23.409      0.008           clock pessimism removed
     23.389     -0.020           clock uncertainty
     23.407      0.018     uTsu  register_file:RegisterFile|n_bit_register:\line2:23:register_i|dflipf:\l0:28:dff_i|s_Q
 Data Arrival Time  :    39.809
 Data Required Time :    23.407
 Slack              :   -16.402 (VIOLATED)
 ===================================================================
