# Proj-C_test1.s

# code/instruction section
.text
	lui   $1, 0x00001001	
	addi  $2,  $0, 1
	addi  $3,  $0, 3 
	addi  $4,  $0, 4
	ori   $8, $1, 0x00000000
	addi  $5,  $0, 5
	add   $6,  $0, 6
	addiu $7,  $0, -7
	j next
	sll   $0, $0, 0 #NOP
	
next:
	addu  $12,  $4, $4
	and   $9,  $5, $4
	andi  $10, $9, 8
	sw    $1, 0($8)         
	sw    $2, 4($8) 
	nor   $12, $0, $5
	xor   $13, $0, $2
	xori  $14, $3, 45
	lw 	  $9, 0($8)         
	lw    $10, 4($8)
	or    $5, $3, $6
	slt   $1, $3, $4		
	slti  $2, $4, 3
	sltiu $3, $5, -99
	sltu  $4, $6, $5
	sll   $5, $12, 13
	srl   $6, $13, 31
	sra   $7, $12, 3
	sllv  $8, $1, $3
	srlv  $9, $2, $3
	srav  $10, $3, $4
	addi  $1, $0, 1
	addi  $2, $0, 2
	addi  $3, $0, 3
	addi  $4, $0, 4
	addi  $5, $0, 5
	addi  $6, $0, 6
	sub   $7, $3, $1
	subu  $8, $4, $2
	jal fun
	sll   $0, $0, 0 #NOP
	beq $0, $0 loop	
	sll   $0, $0, 0 #NOP
	
fun:
	sll   $0, $0, 0 #NOP
	sll   $0, $0, 0 #NOP
	jr $ra
	sll   $0, $0, 0 #NOP
	
loop:
	sub $4, $4, $1
	sll   $0, $0, 0 #NOP
	sll   $0, $0, 0 #NOP
	sll   $0, $0, 0 #NOP
	bne $1, $4, loop
	sll   $0, $0, 0 #NOP

exit:
	addi  $2,  $0,  10              # Place "10" in $v0 to signal an "exit" or "halt"
	syscall                         # Actually cause the halt
