# test.s

.text
	addi  $1, $0, 1
	addi  $2, $0, 2
	addi  $3, $0, 3

loop1:
	sub $3, $3, $1
	bne $1, $3, loop1

addi $4, $0, 4
addi $5, $0, 3

loop2:
	add $5, $5, $1
	beq $5, $4, loop2
	
jal fun
j exit

fun:
	ori $s2 $zero 0x1234
	jr $ra

exit:  
	addi $2, $0, 10
	syscall   