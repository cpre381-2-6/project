# Proj-B_test1.s

# data section
.data
base: .space 0

# code/instruction section
.text
addi  $1, $0, 1 		
addi  $2, $0, 2 		
add   $3, $1, $2		
addiu $4, $3, 4			
addu  $5, $4, $3		
and   $6, $5, $4        
andi  $7, $6, 10		
la    $8, base          
sw    $1, 0($8)         
sw    $2, 4($8)         
lw 	  $9, 0($8)         
lw    $10, 4($8)        
lui   $11, 16			
nor   $12, $0, $1   	
xor   $13, $12, $11     
xori  $14, $13, 7       
or 	  $15, $14, $13   	
ori   $16, $15, 10		
slt   $1, $16, $15		
slti  $2, $16, -1555 	
sltiu $2, $16, -1555 	
sltu  $1, $16, $15		
sll   $3, $2, 7			
srl   $4, $3, 2			
sra   $5, $4, 3			
sub   $3, $1, $2		
subu  $4, $3, 4			
addi  $2, $0, -1000  	
addi  $3, $0, 3			
sllv  $7, $3, $3		
srlv  $8, $3, $3
srav  $8, $3, $3
sllv  $7, $2, $3		
srlv  $8, $2, $3
srav  $8, $2, $3
add   $9, $9, $10

addi  $2,  $0,  10              # Place "10" in $v0 to signal an "exit" or "halt"
syscall                         # Actually cause the halt